FROM python:3.10-slim
RUN apt-get update && apt-get install --no-install-recommends -y bash nano mc libmagic1 git

RUN mkdir /src
WORKDIR /src
COPY . /src/

RUN pip install --upgrade pip
RUN pip install -r requirements/prod.txt

RUN python manage.py collectstatic --settings=altapi.settings.dev --noinput

# run gunicorn
CMD exec gunicorn altapi.wsgi_docker:application --bind 0.0.0.0:8000 --workers 4 --timeout 300


