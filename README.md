# altapi

This is a temporary repo to experiment with altapi in Docker
Once it works, the changes will be merged back into the main ALTA repo
https://git.astron.nl/astron-sdc/ALTA

### DEV
* http://localhost:8000/altapi/observations

### TEST (sdc-dev)
This uses altapi on ``sdc-dev.astron.nl``, which connects to the altadb database on ``alta-acc-db.astron.nl``
* https://sdc-dev.astron.nl/altapi/observations
