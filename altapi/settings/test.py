from altapi.settings.base import *

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '192.168.106.11']
CORS_ORIGIN_ALLOW_ALL = True

TEST_RUNNER = 'xmlrunner.extra.djangotestrunner.XMLTestRunner'
TEST_OUTPUT_VERBOSE = 2
TEST_OUTPUT_DESCRIPTIONS = False
TEST_OUTPUT_FILE_NAME = 'testReport.xml'
