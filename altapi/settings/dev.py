from altapi.settings.base import *

debug = True

ALLOWED_HOSTS = [ '*' ]
CORS_ORIGIN_ALLOW_ALL = True

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'rest_framework.authtoken',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.api',
    'rest_framework',
    'django_filters',
    'corsheaders',
    #'django_extensions'
]

#####################################################
# These settings mainly deal with https.
# See http://django-secure.readthedocs.io/en/latest/middleware.html
# Check the warning and instructions with:
# (.env) alta@/var/.../altapi$ ./manage.py check --deploy --settings=altapi.settings.prod
#####################################################
# Assume SSL is correctly set up.
SSL_ENABLED = False
if SSL_ENABLED:
    # True: Django now checks that cookies are ONLY sent over SSL.
    # https://docs.djangoproject.com/en/1.11/ref/settings/#session-cookie-secure
    SESSION_COOKIE_SECURE = True
    # True: Django now checks that csrf tokens are ONLY sent over SSL.
    # https://docs.djangoproject.com/en/1.11/ref/settings/#csrf-cookie-secure
    CSRF_COOKIE_SECURE = True
    # True: Always redirect requests back to https (currently ignored as Nginx should enforces https).
    #       Alternatively, enable and add set SECURE_PROXY_SSL_HEADER.
    SECURE_SSL_REDIRECT = False
    # Setting this to a non-zero value, will default the client UA always to connect over https.
    # Unclear how or if this possibly affects other *.astron.nl domains. Especially, if these do
    # not support https whether this option then breaks those http-only locations.
    # SECURE_HSTS_SECONDS = 31536000

# True: Enables a header that disables the UA from 'clever' automatic mime type sniffing.
# http://django-secure.readthedocs.io/en/latest/settings.html#secure-content-type-nosniff
# https://stackoverflow.com/questions/18337630/what-is-x-content-type-options-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = True

# True: Enables a header that tells the UA to switch on the XSS filter.
# http://django-secure.readthedocs.io/en/latest/middleware.html#x-xss-protection-1-mode-block
SECURE_BROWSER_XSS_FILTER = True

# Prevents the site from being deployed within a iframe.
# This prevent click-jacking attacks.
# See; https://docs.djangoproject.com/en/1.11/ref/clickjacking/
X_FRAME_OPTIONS = 'DENY'
#####################################################

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'alta',
         'HOST': 'localhost',
         'PORT': '5432',
         'USER': 'atdb_admin',
         'PASSWORD': 'atdb123',
    },

}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

#STATIC_URL = '/alta-static/'
#STATIC_ROOT = 'R:\\source\\javascript\\alta-static\\'
#MY_STATIC_HOST = 'http://localhost'

#MEDIA_URL = '/media/'
#MEDIA_ROOT = STATIC_ROOT + '/media'
