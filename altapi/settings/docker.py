from altapi.settings.base import *

ALLOWED_HOSTS = [ '*' ]
CORS_ORIGIN_ALLOW_ALL = True
# Import production setting must remain False.

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DATABASE_NAME'],
        'HOST': os.environ['DATABASE_HOST'],
        'PORT': os.environ['DATABASE_PORT'],
        'USER' : os.environ['DATABASE_USER'],
        'PASSWORD' : os.environ['DATABASE_PASSWORD'],
    },
}

#####################################################
# These settings mainly deal with https.
# See http://django-secure.readthedocs.io/en/latest/middleware.html
# Check the warning and instructions with:
# (.env) alta@/var/.../altapi$ ./manage.py check --deploy --settings=altapi.settings.prod
#####################################################
# Assume SSL is correctly set up.
SSL_ENABLED = True
if SSL_ENABLED:
    # True: Django now checks that cookies are ONLY sent over SSL.
    # https://docs.djangoproject.com/en/1.11/ref/settings/#session-cookie-secure
    SESSION_COOKIE_SECURE = True
    # True: Django now checks that csrf tokens are ONLY sent over SSL.
    # https://docs.djangoproject.com/en/1.11/ref/settings/#csrf-cookie-secure
    CSRF_COOKIE_SECURE = True
    # True: Always redirect requests back to https (currently ignored as Nginx should enforces https).
    #       Alternatively, enable and add set SECURE_PROXY_SSL_HEADER.
    SECURE_SSL_REDIRECT = False
    # Setting this to a non-zero value, will default the client UA always to connect over https.
    # Unclear how or if this possibly affects other *.astron.nl domains. Especially, if these do
    # not support https whether this option then breaks those http-only locations.
    # SECURE_HSTS_SECONDS = 31536000

# True: Enables a header that disables the UA from 'clever' automatic mime type sniffing.
# http://django-secure.readthedocs.io/en/latest/settings.html#secure-content-type-nosniff
# https://stackoverflow.com/questions/18337630/what-is-x-content-type-options-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = True

# True: Enables a header that tells the UA to switch on the XSS filter.
# http://django-secure.readthedocs.io/en/latest/middleware.html#x-xss-protection-1-mode-block
SECURE_BROWSER_XSS_FILTER = True

# Prevents the site from being deployed within a iframe.
# This prevent click-jacking attacks.
# See; https://docs.djangoproject.com/en/1.11/ref/clickjacking/
X_FRAME_OPTIONS = 'DENY'
#####################################################

# Specific path configuration for the Acceptance and Production environments
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

# STATIC_URL = '/static/'
# STATIC_ROOT = os.path.dirname(BASE_DIR) + STATIC_URL

# media files (used for uploading via the app)
# MEDIA_URL = '/media/'
# Overrule base setting, point to directory on data partition that is shared with the nging server
MEDIA_ROOT = '/data/share/alta-static/media'
