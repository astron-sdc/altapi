from altapi.settings.base import *
import os

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'alta_admin_test_ci',
         'PASSWORD': 'alta123_test_ci',
         'NAME': 'alta_test_ci',
         'HOST': 'postgres',
         'PORT': '5432',
    },
}