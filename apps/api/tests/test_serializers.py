from django.test import TestCase
# app imports
from ..models import *

class AgentSerialization(TestCase):
    """Unit-tests for Agent serialization and validation.
    """
    def setUp(self):
        Agent.objects.create(name="my title", externalRef="my_external_ref")

    def test_data_serialization_to_agent(self):
        #Implement testcase for serializing data to agent.
        self.assertEqual("a", "a")


class ProcessSerialization(TestCase):
    """Unit-tests for Process serialization and validation.
    """
    def setUp(self):
        myAgent = Agent.objects.create(name="my title", externalRef="my_external_ref")
        Process.objects.create(name="my_name", description="my_description", version="1.0", uri="my_uri", ownedByAgent=myAgent)

    def test_data_serialization_to_process(self):
        #Implement testcase for serializing data to process.
        self.assertEqual("a", "a")


class ActivitySerialization(TestCase):
    """Unit-tests for Activity serialization and validation.
    """
    def setUp(self):
        myDateTime = datetime.now()
        myProject = Project.objects.create(externalRef="my_external_ref", rights="public")
        myAgent = Agent.objects.create(name="agent", externalRef="1")
        myProcess = Process.objects.create(name="my_name", description="my_description", version="1.0", uri="my_uri",
                                           type="observation",ownedByAgent=myAgent)
        Activity.objects.create(runId="12345", startTime=myDateTime, endTime=myDateTime, rights='public',
                                project=myProject, process=myProcess, associatedWithAgent=myAgent)

    def test_data_serialization_to_activity(self):
        #Implement testcase for serializing data to activity.
        self.assertEqual("a", "a")


class ProjectSerialization(TestCase):
    """Unit-tests for Project serialization and validation.
    """
    def setUp(self):
        Project.objects.create(externalRef="my_external_ref")

    def test_data_serialization_to_activity(self):
        #Implement testcase for serializing data to Project.
        self.assertEqual("a", "a")


class DataProductSerialization(TestCase):
    """Unit-tests for data-product serialization and validation.
    """
    def setUp(self):
        myAgent = Agent.objects.create(name="my title", externalRef="my_external_ref")
        myProject = Project.objects.create(externalRef="my_external_ref")

        DataProduct.objects.create(dataProductType=DataProduct.TYPE_CUBE, project=myProject, contactAgent=myAgent)

    def test_data_serialization_to_dp(self):
        #Implement testcase for serializing data to dataproduct.
        self.assertEqual("a", "a")