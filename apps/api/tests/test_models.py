from django.test import TestCase
# app imports
from ..models import *
from django.utils.timezone import datetime


class AgentTestModel(TestCase):
    """Unit-tests on the Agent class.
    """
    def test_agent_defaults(self):
        """Test the default agent settings.
        """
        agent = Agent()
        self.assertEqual(agent.name, Agent.DEFAULT_AGENT_NAME)
        self.assertEqual(agent.externalRef, Agent.DEFAULT_AGENT_REF)

    def test_agent_labels(self):
        """Test the labels for Agent.
        """
        agent = Agent(name="my title", externalRef="my_external_ref")
        self.assertEqual(agent.name, "my title")
        self.assertEqual(agent.externalRef,"my_external_ref")


class ProcessTestModel(TestCase):
    """Unit-tests on the Process class.
    """
    def test_process_defaults(self):
        """Test the default process settings.
        """
        process = Process()
        self.assertEqual(process.name, Process.DEFAULT_PROCESS_NAME)
        self.assertEqual(process.description, Process.DEFAULT_PROCESS_DESCRIPTION)
        self.assertEqual(process.version, Process.DEFAULT_PROCESS_VERSION)
        self.assertEqual(process.uri, Process.DEFAULT_PROCESS_URI)
        self.assertEqual(process.ownedByAgent, None)


    def test_process_labels(self):
        """Test the labels for the Process.
        """
        myAgent = Agent()
        process = Process(name="my name", description="my_description", version="1.0", uri="my_uri", ownedByAgent=myAgent)
        self.assertEqual(process.name, "my name")
        self.assertEqual(process.description, "my_description")
        self.assertEqual(process.version, "1.0")
        self.assertEqual(process.uri,"my_uri")
        self.assertEqual(process.ownedByAgent, myAgent)

class ActivityTestModel(TestCase):
    """Unit-tests on the Activity class.
    """
    def test_activity_defaults(self):
        """Test the default activity settings.
        """
        activity = Activity()
        self.assertEqual(activity.runId, Activity.DEFAULT_ACTIVITY_RUNID)
        self.assertEqual(activity.process, None)
        self.assertEqual(activity.associatedWithAgent, None)

    def test_activity_labels(self):
        """Test the labels for the Activity.
        """
        myAgent = Agent()
        myProject = Project.objects.create(externalRef="my_external_ref")
        myProcess = Process(type="observation")
        myDateTime = datetime.now()
        activity = Activity(runId=10, startTime=myDateTime, endTime=myDateTime, project=myProject, process=myProcess, associatedWithAgent=myAgent)
        self.assertEqual(activity.runId, 10)
        self.assertEqual(activity.startTime, myDateTime)
        self.assertEqual(activity.endTime, myDateTime)
        self.assertEqual(activity.process, myProcess)
        self.assertEqual(activity.associatedWithAgent, myAgent)

class ProjectTestModel(TestCase):
    """Unit-tests on the Project class.
    """
    def test_project_defaults(self):
        """Test the default project settings.
        """
        project = Project()
        self.assertEqual(project.externalRef, Project.DEFAULT_PROJECT_EXTERNAL_REF)

    def test_project_labels(self):
        """Test the labels for Project.
        """
        project = Project(externalRef="my externalref")
        self.assertEqual(project.externalRef, "my externalref")

class DataProductTestModel(TestCase):
    """Unit-tests on the DataProduct class.
    """
    def test_dp_defaults(self):
        """Test the default data-product settings.
        """
        dp = DataProduct()
        self.assertEqual(dp.title, DataProduct.DEFAULT_TITLE)
        self.assertEqual(dp.dataProductType, DataProduct.TYPE_IMAGE)

    def test_dp_labels(self):
        """Test the labels for the data-product.
        """
        myAgent = Agent()
        myProject = Project()
        dp = DataProduct(title="my title", dataProductType = DataProduct.TYPE_EVENT, project=myProject, contactAgent=myAgent)
        self.assertEqual(dp.title, "my title")
        self.assertEqual(dp.dataProductType, DataProduct.TYPE_EVENT)

    def test_dp_pointing(self):
        """Test the pointing for the data-product.
        """
        dp = DataProduct(RA = 123.456, dec = 65.432, fov = 0.123, equinox = 'J2000' )
        self.assertEqual(str(dp.RA), "123.456")
        self.assertEqual(str(dp.dec), "65.432")
        self.assertEqual(str(dp.fov), "0.123")
        self.assertEqual(str(dp.equinox), "J2000")

    def test_dp_title(self):
        """Test the title for the data-product.
        """
        dp = DataProduct(title="my data-product")
        self.assertEqual(str(dp.title), "my data-product")

class DataProductTestDB(TestCase):
    """Unit/Integration-tests for DataProduct to Database.
    """
    def setUp(self):
        myAgent =  Agent.objects.create(name="agent", externalRef="123")
        myProject = Project.objects.create(externalRef="my_project",rights="public")
        DataProduct.objects.create(dataProductType=DataProduct.TYPE_CUBE,project=myProject, contactAgent=myAgent)
        DataProduct.objects.create(dataProductType=DataProduct.TYPE_IMAGE,project=myProject, contactAgent=myAgent)

    def test_data_product(self):
        """Test the data-product store to the data-base
        otherwise remove.
        """
        # myAgent = Agent(externalRef="123")
        # myProject = Project(externalRef="my_project")
        # cube = DataProduct.objects.get(dataProductType=DataProduct.TYPE_CUBE, project=myProject, contactAgent=myAgent)
        # image = DataProduct.objects.get(dataProductType=DataProduct.TYPE_IMAGE, project=myProject, contactAgent=myAgent)
        # self.assertEqual(cube.dataProductType, DataProduct.TYPE_CUBE)
        # self.assertEqual(image.dataProductType, DataProduct.TYPE_IMAGE)