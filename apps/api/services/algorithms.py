"""
    File name: algorithms.py
    Author: Nico Vermaas - Astron
    Date created: 2019-08-06
    Description:  Business logic for ALTA. These query functions are called from the views (views.py).
"""

import logging
from django.utils.timezone import datetime
from django.db import connection
from ..models import ReleaseActivity, Activity, DataProduct, Release, Project, Observation
from .alta_utils import timeit
from django.conf import settings
from django.db.models import Q

logger = logging.getLogger(__name__)

def get_freeze():
    """
    Query that returns a list activities that need to be moved to cold storage by irods
    example: https://alta.astron.nl/altapi/get-freeze
    :return:
    """

    result = get_storage(locality_status='inarchive',locality_policy__icontains='cold',quality='good',scrub_timestamp_lte=None)
    return result


def get_prune():
    """
    Query that returns a dictionary of activities (key) that can be pruned.
    Pruning means deleting all beams except the one which equals with the target name (name convention!!!).
    For example
     If target is "3C196_27" then all *B_0??.MS are removed from the observation, only the *B_027.MS will remain.
    The quality must been verified (good) and locality_status inarchive. Also the "calibrator_target"
    of the observation (not activity) must be "calibrator".

    example: https://alta.astron.nl/altapi/get-prune
    :return: A Dictionary with taskid as value and beam number as key
    """
    calibrator_observations = Observation.objects.filter(locality_status='inarchive',
                                                         process__uri='apertif_calibration_observation')
    result = {}
    # Check if the number of dataproducts > 1 otherwise it is already pruned
    # Get the beamdID of the observation which is part of the target '_<beamID>'
    # The <beamID> string value must be a digit otherwise it should not be pruned!
    for observation in calibrator_observations:
        my_dict = {}
        try:
            target = observation.target
            dps = observation.generatedEntities.all()
            size = len(dps)

            if size > 1:
                beamid = target.rsplit('_', 1)[1]
                if beamid.isdigit():
                    my_dict[observation.runId] = beamid
                    result.update(my_dict)
        except:
            logger.info('error during determine pruning list ' + observation.runId)
            pass
    return result


def get_marked_for_deletion():
    """
    Query that returns a list activities that need to be removed from ALTA by irods
    example: https://alta.astron.nl/altapi/get-marked_for_deletion
    :return:
    """

    result = get_storage(locality_status='inarchive', quality='bad',scrub_timestamp_lte=None)
    return result


def get_scrub():
    """
    Query that returns a list of activities that need to be scrubbed from irods
    example: https://alta.astron.nl/altapi/get-scrub
    :return:
    """

    now = datetime.now()
    result = get_storage(locality_status='replicated',locality_policy__icontains='cold',quality='good',scrub_timestamp_lte=now)
    return result


def get_prepared_coldstorage():
    """
    Query that returns a list of activities that need to be prepared for coldstorage
    example: https://alta.astron.nl/altapi/get-prepared-coldstoarge
    :return:
    """

    now = datetime.now()
    result = get_storage(locality_status='tarcreated',locality_policy__icontains='cold_tape',quality='good',scrub_timestamp_lte=None)

    return result


def get_storage(locality_status='', locality_policy__icontains='', quality='', scrub_timestamp_lte=''):
    """
    Query for irods that gives back the observations that need to be tarred, replicated or moved to 'cold storage'.
    note: this query does not go through the DRF,
          but the parameters are written in Django filter format for clarity about their purpose

    :param locality_status: possible values: 'inarchive'
    :param locality_policy: possible values 'archive, cold, cold_disk, cold storage'
    :param quality:  possible values 'good'
    :param scrub_timestamp_lte: the time after which the data will be scrubbed
    :return: list of runid's

    examples: /altapi/get-storage?scrub_timestamp_lte=2019-08-08T06:00:00Z
    """

    #logger.info('get_storage(' + locality_status + ',' + locality_policy__icontains + ',' + quality + ')')

    # get all the observations in the given time range
    if scrub_timestamp_lte!=None:
        activities = Activity.objects.filter(locality_policy__icontains=locality_policy__icontains,
                                             locality_status=locality_status,
                                             quality=quality,
                                             scrub_timestamp__lte=scrub_timestamp_lte)
    else:
        activities = Activity.objects.filter(locality_policy__icontains=locality_policy__icontains,
                                             locality_status=locality_status,
                                             quality=quality)
    result = []
    for activity in activities:
        result.append(activity.runId)

    return result


def put_locality(taskid,locality):
    """
    put the given locality to all the dataproducts generated by activity with taskid
    :param taskid:
    :param locality:
    :return:
    """

    dps = DataProduct.objects.filter(generatedByActivity__runId=taskid)

    result=[]
    for dp in dps:
        dp.locality=locality
        dp.save()
        result.append(dp.name)

    return result


def uniqify(seq):
    # Not order preserving
    keys = {}
    for e in seq:
        keys[e] = 1
    return keys.keys()


def append_locality(taskid,locality):
    """
    append the given locality to all the dataproducts generated by activity with taskid
    :param taskid:
    :param locality:
    :return:
    """

    dps = DataProduct.objects.filter(generatedByActivity__runId=taskid)

    result=[]
    for dp in dps:
        # add the new locality to the list...
        localities = dp.locality.split(',')
        localities.append(locality)

        # ...uniqify the list...
        localities = uniqify(localities)

        # convert it back to a comma separated string and save it.
        new_locality = ','.join(localities)

        # cut off leading ','
        if new_locality.startswith(','):
            new_locality = new_locality[1:]

        dp.locality=new_locality
        dp.save()
        result.append(dp.name)

    return result


def remove_locality(taskid,locality):
    """
    remove the given locality from all the dataproducts generated by activity with taskid
    :param taskid:
    :param locality:
    :return:
    """

    dps = DataProduct.objects.filter(generatedByActivity__runId=taskid)

    result=[]
    try:
        for dp in dps:
            # add the new locality to the list...
            localities = dp.locality.split(',')
            localities.remove(locality)

            # convert it back to a comma separated string and save it.
            new_locality = ','.join(localities)

            dp.locality=new_locality
            dp.save()
            result.append(dp.name)
    except:
        pass
    return result


def get_coldstorage_overview():
    """
    execute a few queries to find which activities are where in the cold storage workflow
    :param taskid:
    :param locality:
    :return:
    """
    try:

        results = {}

        failed = Activity.objects.filter(locality_status='failed').count()
        results['failed'] = failed

        scrubbed = Activity.objects.filter(locality_status='scrubbed').count()
        results['scrubbed'] = scrubbed

        scrubbing = Activity.objects.filter(locality_status='scrubbing').count()
        results['scrubbing'] = scrubbing

        to_scrub = get_scrub()
        results['to_scrub'] = len(to_scrub)

        replicated = Activity.objects.filter(locality_status='replicated').count()
        results['replicated'] = replicated

        replicating = Activity.objects.filter(locality_status='replicating').count()
        results['replicating'] = replicating

        tarcreated = Activity.objects.filter(locality_status='tarcreated').count()
        results['tarcreated'] = tarcreated

        tarring = Activity.objects.filter(locality_status='creatingtar').count()
        results['tarring'] = tarring

        to_freeze = get_freeze()
        results['to_freeze'] = len(to_freeze)

        # to_prune = get_prune()
        # results['to_prune'] = len(to_prune)


    except:
        pass
    return results


@timeit
def mark_as_release_version1(taskid_from, taskid_to, release_id):
    """
    Mark a range of taskid's as belonging to a certain release
    example: # https://alta.astron.nl/altapi/mark-as-release?taskid_from=190801001&taskid_to=190901001&release_id=SVC_2019

    info: https://blog.labdigital.nl/working-with-huge-data-sets-in-django-169453bca049

    """

    #logger.info('mark_as_release(' + str(taskid_from) + ',' + str(taskid_to) + ','+ str(release_id) + ')')

    # this is the release object that we aim for
    release = Release.objects.get(release_id=release_id)

    # get the activities defined by the from/to range
    activities = ReleaseActivity.objects.filter(runId__gte=taskid_from, runId__lte=taskid_to, type='observation')
    # activities = ReleaseActivity.objects.defer('usedEntities').filter(runId__gte=taskid_from, runId__lte=taskid_to).prefetch_related('generatedEntities')

    # disconnect the signal handlers (for performance)
    marked_observations = 0

    for activity in activities:
        if activity.release!=release:
            activity.release=release
            marked_observations = marked_observations + 1

            # note: this 'activity' is of the supertype 'ReleaseActivity' which is not connected to a signal handler
            # for performance reasons.
            activity.save()

            # use a raw sql update for all dataproducts that have this observation as origin
            if activity.type=='observation':
                # propagate the release_id
                datasetID = activity.datasetID
                release_id = activity.release.id
                logger.info(str(marked_observations) + ' - activity :' + activity.runId + ', datasetID: ' + str(
                    datasetID) + ", release_id: " + str(release_id))

                with connection.cursor() as cursor:
                    # update all dataproducts
                    query = "UPDATE api_dataproduct SET release_id=" + str(release_id) + " WHERE \"datasetID\" LIKE '%" + str(datasetID) + "%' AND NOT release_id=" + str(release_id);
                    logger.info(query)
                    cursor.execute(query)

    return marked_observations


def mark_as_release_version2(taskid_from, taskid_to, release_id):
    """
    Mark a range of taskid's as belonging to a certain release
    example: # https://alta.astron.nl/altapi/mark-as-release?taskid_from=190801001&taskid_to=190901001&release_id=SVC_2019

    This version2 works the fastest, but it marks all the dataproducts belonging to a DATASET_ID,
    so does not work per pipeline.
    """

    logger.info('mark_as_release(' + str(taskid_from) + ',' + str(taskid_to) + ','+ str(release_id) + ')')

    # this is the release object that we aim for
    release = Release.objects.get(release_id=release_id)

    # get the activities defined by the from/to range
    observations = ReleaseActivity.objects.filter(runId__gte=taskid_from, runId__lte=taskid_to, type='observation').filter(~Q(release__id=release.id))

    marked_observations = 0
    marked_activities = 0
    marked_dataproducts = 0

    for observation in observations:

        marked_observations = marked_observations + 1

        datasetID = observation.datasetID
        # release_id = observation.release.id
        logger.info(str(marked_observations)+' - observation :'+observation.runId+', datasetID: '+str(datasetID)+", release_id: "+str(release.id))

        # use a raw sql update for all dataproducts that have this observation as origin
        with connection.cursor() as cursor:
            # update all dataproducts
            query = "UPDATE api_dataproduct SET release_id=" + str(release.id) + " WHERE \"datasetID\" LIKE '%" + str(datasetID) + "%' AND (NOT release_id=" + str(release.id) + "OR release_id is NULL)";
            logger.info(query)
            cursor.execute(query)
            try:
                marked_dataproducts = marked_dataproducts + cursor.rowcount
            except:
                # no dataproducts updated, do nothing
                pass

    # use a raw sql update for all activities
    with connection.cursor() as cursor:
        # update all releases
        query = "UPDATE api_activity SET release_id=" + str(release.id) + " WHERE \"runId\" >= '" + str(taskid_from) + "' AND \"runId\" <= '" + str(taskid_to) + "' AND (NOT release_id=" + str(release.id) + "OR release_id is NULL)" ;
        logger.info(query)
        cursor.execute(query)
        try:
            marked_activities = cursor.rowcount
        except:
            marked_activities = 0

    return marked_observations, marked_activities, marked_dataproducts



@timeit
def mark_as_release(taskid_from, taskid_to, release_id):
    """
    Mark a range of taskid's as belonging to a certain release
    example: # https://alta.astron.nl/altapi/mark-as-release?taskid_from=190801001&taskid_to=190901001&release_id=SVC_2019

    info: https://blog.labdigital.nl/working-with-huge-data-sets-in-django-169453bca049

    """

    logger.info('mark_as_release(' + str(taskid_from) + ',' + str(taskid_to) + ','+ str(release_id) + ')')

    # this is the release object that we aim for
    release = Release.objects.get(release_id=release_id)

    # get the activities defined by the from/to range
    # ReleaseActivity is a subclass of Activity that is saved only to update release information.
    activities = ReleaseActivity.objects.filter(runId__gte=taskid_from, runId__lte=taskid_to).filter(~Q(release__id=release.id))

    marked_observations = 0
    marked_activities = 0
    marked_dataproducts = 0

    for activity in activities:

        entities = activity.generatedEntities.all()
        try:
            for entity in entities:
                myDataProduct = DataProduct.objects.get(PID=entity.PID)
                myDataProduct.release = release
                myDataProduct.save()
                marked_dataproducts = marked_dataproducts + 1

            activity.release = release
            activity.save()
            marked_activities = marked_activities + 1

            if activity.type == "observation":
                marked_observations = marked_observations + 1

        except Exception as e:
            logger.error(e)

    return marked_observations, marked_activities, marked_dataproducts


@timeit
def mark_as_release_version4(taskid_from, taskid_to, release_id):
    """
    Mark a range of taskid's as belonging to a certain release
    example: # https://alta.astron.nl/altapi/mark-as-release?taskid_from=190801001&taskid_to=190901001&release_id=SVC_2019

    This version4 was an attempt to use a direct sql query to update all the dataproducts
    per activity. In the end the UPDATE doesn't work (the SELECT did).
    Remove later, but keep for now as a monument to a failed attempt.
    """

    logger.info('mark_as_release(' + str(taskid_from) + ',' + str(taskid_to) + ','+ str(release_id) + ')')

    # this is the release object that we aim for
    release = Release.objects.get(release_id=release_id)

    # get the activities defined by the from/to range
    # ReleaseActivity is a subclass of Activity that is saved only to update release information.
    activities = ReleaseActivity.objects.filter(runId__gte=taskid_from, runId__lte=taskid_to).filter(~Q(release__id=release.id))

    marked_observations = 0
    marked_activities = 0
    marked_dataproducts = 0

    for activity in activities:
        # alas, this works for a SELECT statement but not for a UPDATE statement
        query = "UPDATE api_dataproduct SET release_id=" + str(release.id) + ' '
        query = query + \
                'FROM "api_dataproduct"' \
                'INNER JOIN "api_dataentity" ' \
                'ON ("api_dataproduct"."dataentity_ptr_id" = "api_dataentity"."entity_ptr_id")' \
                'INNER JOIN "api_entity"' \
                'ON ("api_dataentity"."entity_ptr_id" = "api_entity"."altaobject_ptr_id")' \
                'INNER JOIN "api_altaobject"' \
                'ON ("api_entity"."altaobject_ptr_id" = "api_altaobject"."id")' \
                'INNER JOIN "api_activity_generatedEntities" ' \
                'ON ("api_entity"."altaobject_ptr_id" = "api_activity_generatedEntities"."entity_id")' \
                'WHERE "api_activity_generatedEntities"."activity_id" = '

        query = query + "'" + str(activity.id) +"'"
        try:
            with connection.cursor() as cursor:
                # logger.info(query)
                cursor.execute(query)
                try:
                    marked_dataproducts = marked_dataproducts + cursor.rowcount
                except:
                    # no dataproducts updated, do nothing
                    pass

            activity.release = release
            activity.save()
            marked_activities = marked_activities + 1
        except Exception as e:
            logger.error(e)

    return marked_observations, marked_activities, marked_dataproducts


def get_activity_details(runid):
    """
    retrieve details of dataproducts generated by an activity with 'runid'
    :param runid:
    :return:
    """

    dps = DataProduct.objects.filter(generatedByActivity__runId=runid).order_by('name')
    results = {}
    dps_records = []
    for dp in dps:
        record = {}
        record['name'] = dp.name
        record['pid'] = dp.PID
        record['RA'] = dp.RA
        record['dec'] = dp.dec
        dps_records.append(record)

    activity_record = {'runid' : runid}
    results['activity'] = activity_record
    results['dataproducts'] = dps_records

    return results


def get_dataproducts_storageref_type_subtype(runid):
    """
    retrieve a list of dataproducts details; storageRef,  type and subtype generated by an activity with 'runid'
    :param runid:
    :return:
    """
    dps = DataProduct.objects.filter(generatedByActivity__runId=runid).order_by('name')
    results = []
    for dp in dps:
        record = {}
        record['name'] = dp.name
        record['storageRef'] = dp.storageRef
        record['dataProductType'] = dp.dataProductType
        record['dataProductSubType'] = dp.dataProductSubType

        results.append(record)
    return results


def get_releases(status):
    """
    Query that returns a list of releases with status 'final'.
    The iRODS side will use that as a signal to create symbolic links containing the release_id.
    example: https://alta.astron.nl/altapi/get-final-releases
    :return:
    """

    # find the final releases
    releases = Release.objects.filter(status=status)
    results = []

    for release in releases:
        result = {}
        result['release_id'] = release.release_id

        # retrieve all runid's for this release
        activities = Activity.objects.filter(release=release)
        runids = []
        for activity in activities:
            runids.append(activity.runId)

        result['runids'] = runids

        results.append(result)

    return results


@timeit
def set_rights_for_project(project, rights):
    """
    set rights for all observations and dataproducts belonging to this project
    use raw sql execution for performance reasons
    :param rights:
    :return:
    """
    logger.info('set_rights_for_project(' + str(project) + ',' + str(rights) + ')')
    # print(str(project)+' '+str(rights))
    project_id = project.id

    with connection.cursor() as cursor:
        # update all activites
        query = "UPDATE api_activity SET rights='" + str(rights) + "' WHERE project_id = " + str(project_id) + " AND NOT rights='" + str(rights)+"'";
        logger.info(query)
        cursor.execute(query)

        # update all dataproducts
        query = "UPDATE api_dataproduct SET rights='" + str(rights) + "' WHERE project_id = " + str(project_id) + " AND NOT rights='" + str(rights) + "'";
        logger.info(query)
        cursor.execute(query)


@timeit
def set_rights_for_release(release, rights):
    """
    set rights for all observations and dataproducts belonging to this release
    :param rights:
    :return:
    """
    logger.info('set_rights_for_release(' + str(release) + ',' + str(rights) + ')')

    release_id =release.id

    # get the project for this release, all dataproducts of that project will get these rights now.
    # (because there is not releaes object per dataproduct, when that is added then set rights per release instead of per project

    project_id = release.project.id
    print(project_id)

    with connection.cursor() as cursor:

        # update all activites
        query = "UPDATE api_activity SET rights='" + str(rights) + "' WHERE release_id = " + str(release_id) + " AND NOT rights='" + str(rights)+"'";
        logger.info(query)
        cursor.execute(query)
        logger.info('chanced_activities = '+str(cursor.rowcount))

        # update all dataproducts
        # dirty, because it now works per 'project'. When 'release object' is also added to dataproducts then alter this to work per release.
        # Good enough for now, the database change/migration can wait until the 'release' object has to be made many-to-many
        # query = "UPDATE api_dataproduct SET rights='" + str(rights) + "' WHERE project_id = " + str(project_id) + " AND NOT rights='" + str(rights) + "'";

        query = "UPDATE api_dataproduct SET rights='" + str(rights) + "' WHERE release_id = " + str(release_id) + " AND NOT rights='" + str(rights) + "'";
        logger.info(query)
        cursor.execute(query)
        logger.info('chanced_dataproducts = '+str(cursor.rowcount))


def handle_dataproduct_thumbnail(myDataProduct):
    """
    Abstracted thumbnail logic for dataproducts.
    This function is called by the signal handlers when a dataproduct is saved by the save_activity signal handler
    Default dataProductSubTypes get a default thumbnail. The configuration for this is in base.py
    None default types get special treatment, the logic is in this function.

    :param myDataProduct:
    :return:

    TESTS:
    [x] test empty thumbnails
        [x] default thumbnails
        [x] specific  thumbnails
    [ ] test existing thumbnails
        [ ] with host
        [ ] without host

    """
    # update thumbnails if needed
    thumbnail = myDataProduct.thumbnail

    # if a thumbnail is provided, but lacking the host, then add the host to it and return.
    # This overrides the default behaviour that follows. Which also means that saving activities later
    # will not change the thumbnail.

    #thumbnail = ""  # force new generation... remove this
    if (thumbnail != ""):
        if (thumbnail.find('http')) < 0:
            thumbnail = settings.MY_STATIC_HOST + thumbnail
        return thumbnail

    # thumbnail is empty, execute the logic to fill it...

    # add default thumbnails for dataproduct types that do not provide specific thumbnails
    thumbnail = settings.DEFAULT_THUMBNAILS.get(myDataProduct.dataProductSubType)


    if thumbnail != None:
        # default thumbnails live in the 'alta-static/images' directory
        thumbnail = 'images/' + thumbnail
    else:
        # if no default thumbnail was found, then it might be one of the specific ones that require
        # special attention

        if myDataProduct.dataProductSubType=='continuumMF':
            # the ingest has uploaded this image to the webserver next to the fits dataproduct
            # the filename is the same as fits, except it is a png.
            # https://alta.astron.nl/alta-static//media/190409015_AP_B030/image_mf_02.png
            thumbnail = settings.MEDIA_URL + myDataProduct.storageRef.replace('fits','png')

    # if thumbnail is still None, then finally add the 'unknown' image
    if thumbnail == None:
        thumbnail = "images/unknown.jpg"

    # add the host
    thumbnail = settings.MY_STATIC_HOST + '/alta-static/' + thumbnail
    logger.info(thumbnail)

    return thumbnail


def append_storageref(PID,storageRef):
    """
    append the given locality to all the dataproducts generated by activity with taskid
    :param PID:
    :param storageref:
    :return:
    """

    dps = DataProduct.objects.filter(PID=PID)

    result=[]
    for dp in dps:
        # add the new locality to the list...
        storageRefs = dp.storageRef.split(',')
        storageRefs.append(storageRef)

        # ...uniqify the list...
        storageRefs = uniqify(storageRefs)

        # convert it back to a comma separated string and save it.
        new_storageRef = ','.join(storageRefs)

        # cut off leading ','
        if new_storageRef.startswith(','):
            new_storageRef = new_storageRef[1:]

        dp.storageRef=new_storageRef
        dp.save()
        result.append(dp.storageRef)

    return result


def remove_storageref(PID, storageRef):
    """
    remove the given storageref from the dataproduct identified by PID
    :param PID:
    :param storageRef:
    :return:
    """

    dps = DataProduct.objects.filter(PID=PID)

    result=[]
    try:
        for dp in dps:
            # add the new locality to the list...
            storageRefs = dp.storageRef.split(',')
            storageRefs.remove(storageRef)

            # convert it back to a comma separated string and save it.
            new_storageRef = ','.join(storageRefs)

            dp.storageRef=new_storageRef
            dp.save()
            result.append(dp.storageRef)
    except:
        pass
    return result


def append_storageref_byname(runid, name, storageRef):
    """
    append the given locality to all the dataproducts generated by activity with taskid
    :param PID:
    :param storageref:
    :return:
    """
    dps = DataProduct.objects.filter(generatedByActivity__runId=runid, name=name)
    result = []
    for dp in dps:
        # add the new locality to the list...
        storageRefs = dp.storageRef.split(',')
        storageRefs.append(storageRef)

        # ...uniqify the list...
        storageRefs = uniqify(storageRefs)

        # convert it back to a comma separated string and save it.
        new_storageRef = ','.join(storageRefs)

        # cut off leading ','
        if new_storageRef.startswith(','):
            new_storageRef = new_storageRef[1:]

        dp.storageRef=new_storageRef
        dp.save()
        result.append(dp.storageRef)
    return result


def remove_storageref_byname(runid, name, storageRef):
    """
    remove the given storageref from the dataproduct identified by PID
    :param PID:
    :param storageRef:
    :return:
    """
    dps = DataProduct.objects.filter(generatedByActivity__runId=runid, name=name)
    result = []
    try:
        for dp in dps:
            # add the new locality to the list...
            storageRefs = dp.storageRef.split(',')
            storageRefs.remove(storageRef)

            # convert it back to a comma separated string and save it.
            new_storageRef = ','.join(storageRefs)

            dp.storageRef=new_storageRef
            dp.save()
            result.append(dp.storageRef)
    except:
        pass
    return result
