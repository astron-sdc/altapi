"""
alta_utils class, for generic ALTA functions
"""
import logging;
from datetime import *
import time
from django.db.models import Q
from apps.api.models import Observation, Project

logger = logging.getLogger(__name__)


def isCalibrator(name):
    """
    check if the provided name is in the (hardcoded) list of defined Apertif Calibrators
    :param: name, the name of a source.
    :return: True, if this is a calibrator
    """
    APERTIF_CALIBRATORS = ['3C48', '3C048', '3C138', '3C147', '3C196', '3C286', '3C295', 'CTD93']
    for calibrator in APERTIF_CALIBRATORS:
        if name.find(calibrator) >= 0:
            return True

    return False


# check if this observation is pointed at a known calibrator.
# If so, define it as a calibrator, otherwise define it as a target
def setCalibratorTarget(this_observation):
    if (isCalibrator(this_observation.target)):
        this_observation.calibrator_target = 'calibrator'
    else:
        this_observation.calibrator_target = 'target'

    this_observation.save()

def findMyCalibrators(this_observation, override = False):
    """
    find calibrators of this target, or targets of this calibrator, up to 15 minutes before my starttime.
    :param (in) observation: the calibrator observation or target observation to search from.
    :param (in) override: If False then calibrators are only added if the target observations has none in the database yet.
    """

    logger.info('findMyCalibrators('+str(this_observation.runId)+')')
    calibratorList = []
    targetRunId = None

    # initialize the variables depending on whether this_observation is a calibrator or a target
    if (isCalibrator(this_observation.target)):
        # if this_observation is a calibrator then add this_observation to the calibratorList.
        logger.info(
            'this observation, ' + str(this_observation.runId) + ', ' + this_observation.target + ', is a calibrator, find matching targets.')
        isSearchForCalibrators = False
        calibratorList.append(this_observation)
    else:
        # this_observation is a target.
        logger.info(
            'this observation, ' + str(this_observation.runId) + ', ' + this_observation.target + ', is a target, find matching calibrators.')
        isSearchForCalibrators = True
        targetRunId = this_observation.runId

        # check if this target already has calibrators in the database,
        # and abort this operation function when that is the case and override = False
        if not override:
            if (len(this_observation.calibrator_sources) > 0):
                logger.info('Aborting operation. Reason: Override = false and '+
                            str(this_observation.runId)+' already has calibrators : '+this_observation.calibrator_sources)
                return

    # search 15 minutes before the start of this observation until 15 minutes after the end.
    # harcoded, but it could be escalated up to a parameter if needed.
    myStartTime = this_observation.startTime
    myEndTime = this_observation.endTime
    delta = timedelta(minutes=int(15))
    startSearch = myStartTime - delta
    endSearch = myEndTime + delta

    # do the search and iterated through the found observations
    found_observations = Observation.objects.filter(Q(endTime__gt=startSearch), Q(startTime__lt=endSearch))
    for found_observation in found_observations:
        logger.info('found observation: '+str(found_observation.runId))

        # because of the possibly random order of the ingests, the related observations (siblings) may not
        # yet exist in the database. The trick is to search for neighbors when both a calibrator or target
        # observation is ingested.

        # find the calibrator preceding this target
        if (isSearchForCalibrators):
            # don't be confused by the term 'target' here, that is the observed source in the observation, which can also be a calibrator.
            if (isCalibrator(found_observation.target)):
                # found a calibrator observation
                logger.info('...adding '+str(found_observation.runId)+' to calibratorList.')
                calibratorList.append(found_observation)

        # alternatively, find the target preceding this calibrator.
        else:
            if (not isCalibrator(found_observation.target)):
                # found a target observation
                targetRunId = found_observation.runId

    # at this point the target and a list of calibrators is known. Save them.

    # first retrieve the target_observation
    if (targetRunId==None):
        logger.info('Aborting operation. Reason: no target found, '+
                    str(this_observation.runId) + ' is probably the preceding Calibration Observation. '
                                                  'It will be found later when the Target Observation is saved.')
        return

    targetObservation = Observation.objects.get(runId=targetRunId)

    # first get the current values
    sources = []
    if (len(targetObservation.calibrator_sources) > 0):
        sources = targetObservation.calibrator_sources.split(",")

    runIds = []
    if (len(targetObservation.calibrator_runIds) > 0):
        runIds = targetObservation.calibrator_runIds.split(",")

    # check if this target already has calibrators in the database,
    # and abort this operation function when that is the case and override = False
    if not override:
       if (len(this_observation.calibrator_sources) > 0):
            logger.info('Aborting operation. Reason: Override = false and ' +
                        str(this_observation.runId) + ' already has calibrators : ' + this_observation.calibrator_sources)
            return

    # not aborted, continue to fill the list.
    for calibrator in calibratorList:
        runIds.append(calibrator.runId)
        sources.append(calibrator.target)

    # make unique
    sources = list(set(sources))
    runIds = list(set(runIds))

    # add calibrators to target
    targetObservation.calibrator_sources = ','.join(sources)
    targetObservation.calibrator_runIds = ','.join(runIds)
    targetObservation.type = 'observation'

    logger.info('save targetObservation ' + targetRunId + ', adding calibrators '+targetObservation.calibrator_sources)
    targetObservation.save()


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('execution time: %r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed


def isMarkedForDeletion(quality):
    """
    The algorithm to determine whether data should be deleted or not.
    based on a string value in the (activity) quality field
    :param quality:
    :return:
    """
    marked_for_deletion = False

    if 'bad' in quality:
        marked_for_deletion = True

    return marked_for_deletion