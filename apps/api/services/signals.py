import logging, pdb;
from django.conf import settings
from django.db.models.signals import pre_save, post_save
from django.core.signals import request_started, request_finished
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType

from .alta_utils import findMyCalibrators, setCalibratorTarget, isMarkedForDeletion
from .algorithms import set_rights_for_project, set_rights_for_release, handle_dataproduct_thumbnail

from apps.api.models import DataProduct, Activity, ReleaseActivity, Observation, Project, Release
#nv: 28may2018, pycharm gives an 'unresolved reference' on apps, but the application seems to work

from rest_framework.authtoken.models import Token

"""
Signals sent from different parts of the backend are centrally defined and handled here.
"""

logger = logging.getLogger(__name__)

#--- USER signals-------------

@receiver(post_save, sender=User)
def init_new_user(sender, instance, signal, created, **kwargs):
    """
    Create an authentication token for new users
    """
    if created:
        logger.info('signal : init_new_user => Token created')
        Token.objects.create(user=instance)

#--- HTTP REQUEST signals-------------

@receiver(request_started)
def request_started_handler(sender, **kwargs):
    logger.debug("signal : request_started")


@receiver(request_finished)
def request_finished_handler(sender, **kwargs):
    logger.debug("signal : request_finished")

#--- ACTIVITY - OBSERVATION signals-------------
@receiver(pre_save, sender=Activity)
def pre_save_activity_handler(sender,  **kwargs):
    logger.debug("signal : pre_save_activity_handler")

# note that both Activity and (its subclass) Observation are intercepted and handled by the same handler function.
# this is needed because both classes can be posted to store Activities (and their generated entities).
@receiver(post_save, sender=Activity)
def post_save_activity_handler(sender,  **kwargs):
    handle_save_activity(sender, **kwargs)

# ReleaseActivity is a subclass of Activity that is saved only to update release information
# We don't want to execute the datasetid algorithms or save all the dataproducts...
# ... in fact, nothing else than 'save()' has to happen, so no signal handler required.
# (Enable this when specific ReleaseActivity functionality has to be added later)
# @receiver(post_save, sender=ReleaseActivity)
# def post_save_release_activity_handler(sender,  **kwargs):
#    handle_save_activity(sender, **kwargs)
#    pass

@receiver(post_save, sender=Observation)
def post_save_observation_handler(sender, **kwargs):
    handle_save_activity(sender, **kwargs)


def handle_save_activity(sender, **kwargs):
    """
    intercept the Activity (or Observation) 'save' operation to check if the datasetID is aboard.
    the receiver decorator automagically 'connects' my_pre_save_activity_handler to the Activity
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    # [ ] test : does empty datasetid lead to new datasetid?
    # [x] test : does empty calibrator list lead ot new calibrator list?

    logger.info("SIGNAL : handle_save_activity("+str(kwargs.get('instance'))+")")

    # get the instance of the object and look for the datasetID
    activity = kwargs.get('instance')
    senderName = ContentType.objects.get_for_model(sender).name

    # ReleaseActivity is a subclass of Activity that is saved only to update release information
    # We don't want to execute the datasetid algorithms or save all the dataproducts dataproducts
    # So skip all that unless the sender is really of the type Activity
    # my_type = type(activity)
    is_release_activity = isinstance(activity,ReleaseActivity)

    if not is_release_activity:
        logger.info('initial (provided) datasetID = [' + activity.datasetID + ']')

        global myDatasetIDs
        myDatasetIDs = set()
        datasetIDs = activity.datasetID

        # if there is no dataset given then generate it by navigating the provenance tree
        # (following the dataproducts back to the target observation)
        if datasetIDs=="":
            fillMyDatasetIDs(activity)

            # convert the result in a list
            if len(myDatasetIDs) > 0:
                datasetIDs = formatDatasetIDs(myDatasetIDs)

            activity.datasetID = datasetIDs
            logger.info('datasetIDs = ' + datasetIDs)

        # if no thumbnail is provided, then use the default for this process.
        if activity.thumbnail=="":
           activity.thumbnail=activity.derived_process_thumbnail

        # add static host if missing (like when the UPLOAD view was used)
        if activity.thumbnail.find('http')<0:
            logger.info('thumbnail = '+activity.thumbnail)
            activity.thumbnail = settings.MY_STATIC_HOST+activity.thumbnail

        # update the datasetID in the generatedDataproducts
        for entity in activity.generatedEntities.all():
            myDataProduct = DataProduct.objects.get(id=entity.id)
            myDataProduct.datasetID=datasetIDs

            # update thumbnails if needed
            myDataProduct.thumbnail = handle_dataproduct_thumbnail(myDataProduct)

            # update 'marked_for_deletion' if needed
            myDataProduct.marked_for_deletion = isMarkedForDeletion(activity.quality)
            myDataProduct.type='dataproduct'
            myDataProduct.save()


    # temporarily disconnect the post_save signal handlers to prevent looping.. then reconnect
    # should this be handled differently? (like overriding save instead of a signal?)

    post_save.disconnect(post_save_activity_handler, sender=sender)
    # post_save.disconnect(post_save_release_activity_handler, sender=sender)
    post_save.disconnect(post_save_observation_handler, sender=sender)

    # do not change an existing activity.type
    # for observations, the sip xml-parser POSTS the initial object als 'observation',
    # but PUTS the update as 'activity' later. So do not change it once it is set.
    if (activity.type=='unknown'):
        activity_type = ContentType.objects.get_for_model(sender).name
        logger.info('activity.save(' + activity.type + ') => ' + activity_type)
        activity.type = activity_type

    activity.save()

    # hook in the search for calibrators in caee of an observation
    # important,
    # This is done where the post_save is temporarily disconnected from the signal handler,
    # because findMyCalibrators might also do a save for related Observations which could
    # otherwise cause a cascade and recursion.
    if senderName=='observation':
        try:
            setCalibratorTarget(activity)
            findMyCalibrators(activity,override=True)

        except Exception as e:
            # an exception in finding calibrators should not break the Ingest.
            # so report the error and do continue.
            logger.error("ERROR in findMyCalibrators: "+str(e))

    post_save.connect(post_save_activity_handler, sender=sender)
    post_save.connect(post_save_observation_handler, sender=sender)
    # post_save.connect(post_save_release_activity_handler, sender=sender)


def formatDatasetIDs(myDatasetIDs):
    """
    format the set of datasetIDs to an easier to handle string for the frontend
    "{'180119119', '18010501'}" => "180119119_18010501"
    :param (in) myDatasetIDs: set with collected datasetIDs
    :returns: formatted string
    """
    datasetIDs = repr(myDatasetIDs)
    datasetIDs = datasetIDs.replace("'", "")
    datasetIDs = datasetIDs.replace("{", "")
    datasetIDs = datasetIDs.replace("}", "")
    datasetIDs = datasetIDs.replace(" ", "")
    # datasets are combined by a '_' because a ',' would interfere with a filter like
    # GET /altapi/activities?datasetID__in=18010501,180119119,18010501_180119119
    datasetIDs = datasetIDs.replace(",", "_")
    return datasetIDs


def fillMyDatasetIDs(activity):
    """
    find a list of datasetIDs by looking at the activity.datasetID of the observation at the start of the provenance tree
    by navigating through the chain of used & generated entities (input/output dataproducts)
    :param (in) activity: The activity for which the root datasetIDs are collected
    """

    logger.info('fillMyDatasetIDs(' + str(activity) + ')')

    process_type = activity.derived_process_type
    logger.info('process_type = ' + process_type);

    if (process_type=='observation'):
        # if the process is an observation, then the root datasetID is its runId.
        # reached the observation level
        datasetID = activity.runId
        logger.info('observation level reached... add to myDatasetIDs : ' +  str(datasetID))
        myDatasetIDs.add(datasetID)

    else:
        # for pipelines, dig deeper (recursive)
        for entity in activity.usedEntities.all():
            logger.info('entity = ' + str(entity))
            # find the activity that created this entity
            parentActivity = findParentActivity(entity)

            # recursive call to go deeper into the provenance tree.
            fillMyDatasetIDs(parentActivity)


def findParentActivity(entity):
    """
    find the Activity that created this Entity
    :param (in) entity: The child Entity who's parent is searched
    :returns: parent Activity that created the Entity
    """

    parentActivity=None
    querySet = entity.generatedByActivity.all()
    if (querySet!=None):
        parentActivity=querySet[0]

    logger.info('parentActivity(' + str(entity) + ') => ' + str(parentActivity))
    return parentActivity



# --- PROJECT signals-------------
@receiver(post_save, sender=Project)
def post_save_project_handler(sender, **kwargs):
    """
    intercept the Project 'save' operation to write its 'rights' to underlying dataproducts.
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    logger.info("SIGNAL : post_save_project_handler(" + str(kwargs.get('instance')) + ")")
    myProject = kwargs.get('instance')

    # new faster mechanism using raw sql queries
    set_rights_for_project(myProject, myProject.rights)


# disabled for now, trying out the faster mechanism
# @receiver(post_save, sender=Project)
def post_save_project_handler_obsolete(sender, **kwargs):
    """
    intercept the Project 'save' operation to write its 'rights' to underlying dataproducts.
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    logger.info("SIGNAL : post_save_project_handler("+str(kwargs.get('instance'))+")")
    myProject = kwargs.get('instance')

    # gather all activities of this project, and if needed change their rights
    activities = Activity.objects.filter(project=myProject)

    for myActivity in activities:

        if myActivity.rights != myProject.rights:
            # to bypass a bug in Windows/python
            s = 'activity '+myActivity.runId + ' : ' + str(myActivity.rights) + ' => ' + myProject.rights
            s.encode("utf-8").decode("ascii")

            logger.info(s)

            myActivity.rights = myProject.rights
            myActivity.save()

    # gather all dataproducts of this project, and if needed change their rights
    dps = DataProduct.objects.filter(project=myProject)

    for myDataProduct in dps:

        if myDataProduct.rights!=myProject.rights:
            # to bypass a bug in Windows/python
            s = 'dataproduct '+myDataProduct.name + ' : ' + str(myDataProduct.rights) + ' => ' + myProject.rights
            s.encode("utf-8").decode("ascii")

            logger.info(s)

            myDataProduct.rights = myProject.rights
            myDataProduct.save()


# release signals

# release signals
@receiver(post_save, sender=Release)
def post_save_release_handler(sender, **kwargs):
    """
    intercept the Release 'save' operation to write its 'rights' to underlying observations and dataproducts.
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    logger.info("SIGNAL : post_save_release_handler("+str(kwargs.get('instance'))+")")
    myRelease = kwargs.get('instance')

    # new faster mechanism using raw sql queries
    set_rights_for_release(myRelease, myRelease.rights)


#@receiver(post_save, sender=Release)
def post_save_release_handler_obsolete(sender, **kwargs):
    """
    intercept the Release 'save' operation to write its 'rights' to underlying observations and dataproducts.
    :param (in) sender: The model class that sends the trigger
    :param (in) kwargs: The instance of the object that sends the trigger.
    """
    logger.info("SIGNAL : post_save_release_handler("+str(kwargs.get('instance'))+")")
    myRelease = kwargs.get('instance')

    # gather all activities of this project, and if needed change their rights
    activities = ReleaseActivity.objects.filter(release=myRelease)

    for myActivity in activities:

        if myActivity.rights != myRelease.rights:
            # to bypass a bug in Windows/python
            s = 'activity '+myActivity.runId + ' : ' + str(myActivity.rights) + ' => ' + myRelease.rights
            s.encode("utf-8").decode("ascii")

            logger.info(s)

            myActivity.rights = myRelease.rights
            myActivity.save()

            # change the rights of all the dataproducts also
            entities = myActivity.generatedEntities.all()
            try:
                for entity in entities:
                    myDataProduct = DataProduct.objects.get(PID = entity.PID)
                    if myDataProduct.rights!=myRelease.rights:
                        # to bypass a bug in Windows/python
                        s = 'dataproduct '+myDataProduct.name + ' : ' + str(myDataProduct.rights) + ' => ' + myRelease.rights
                        s.encode("utf-8").decode("ascii")

                        logger.info(s)

                        myDataProduct.rights = myRelease.rights
                        myDataProduct.save()
            except:
                pass