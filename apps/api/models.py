from django.db import models
from django.utils.timezone import datetime
from datetime import timedelta
from django.conf import settings
import uuid, logging
import os;
from .services.storage import OverwriteStorage

logger = logging.getLogger(__name__)

# models.py is the implementation of the ALTA datamodel:
# https://drive.google.com/file/d/0B5fohp6auM-uOUJCLVZuckFseFU/view?usp=sharing
# usually a pid is given, if this is not the case (like for the unit tests) create one.

RIGHTS_PUBLIC = 'public'
RIGHTS_SECURE = 'secure'
RIGHTS_PROPRIETARY = 'proprietary'
DJANGO_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

def get_defaullt_pid():
    pid = str(uuid.uuid4())
    return pid


class Project(models.Model):
    # properties
    DEFAULT_PROJECT_EXTERNAL_REF = "no external ref"
    DEFAULT_PROJECT_NAME = "no project name"

    # uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    externalRef = models.CharField('externalRef (URI)', max_length=255, default=DEFAULT_PROJECT_EXTERNAL_REF)
    name = models.CharField('name', max_length=80, default=DEFAULT_PROJECT_NAME)
    rights = models.CharField('rights', default=RIGHTS_PUBLIC, max_length=15)

    def __str__(self):
        return str(self.externalRef) + ' - ' + str(self.name)


class Ingest(models.Model):
    # properties
    PID = models.CharField('Persistent Identifier.', default=get_defaullt_pid, null=False, max_length=255)
    timestamp = models.DateTimeField('Timestamp of creation in the database.', default=datetime.now, blank=True)
    status = models.CharField('Status', default="no status", max_length=200)
    username = models.CharField('Title', default="no user", max_length=200)

    def __str__(self):
        return self.PID

    class Meta:
        ordering = ('-timestamp',)


class AltaObject(models.Model):
    # initially a 'rollback' by removing an ingest record will be handled by setting AltaObject.ingest to null
    # later, if deemed safe enough, the objects could be deleted on cascade, but it is safer to do it in 2 steps.
    ingest = models.ForeignKey(Ingest, related_name='ingested', null=True, on_delete=models.CASCADE)
    type = models.CharField(default='unknown', max_length=50, blank=True)

    def __str__(self):
        return str(self.id)


class Agent(models.Model):
    # constants
    DEFAULT_AGENT_NAME = 'No Agent'
    DEFAULT_AGENT_REF = 'none'
    # properties
    name = models.CharField('agent name', default=DEFAULT_AGENT_NAME, max_length=200)
    externalRef = models.CharField('external reference', default=DEFAULT_AGENT_REF, max_length=255)

    def __str__(self):
        return self.name


class Release(models.Model):
    release_id = models.CharField(max_length=100, default="unknown", null=True)
    name = models.CharField(max_length=100)
    releaseDate = models.DateTimeField('release date', default=datetime.now)
    documentation = models.CharField('documentation', max_length=255)
    agent = models.ForeignKey(Agent, related_name='%(class)s_agent', null=True, on_delete=models.SET_NULL)
    project = models.ForeignKey(Project, null=True, on_delete=models.SET_NULL)
    rights = models.CharField('rights', default=RIGHTS_PUBLIC, max_length=15)
    status = models.CharField('status', default="open", max_length=50) # 'open', 'final', 'published'

    def __str__(self):
        return str(self.release_id)

class Process(models.Model):
    # constants
    DEFAULT_PROCESS_NAME = 'Observation'
    DEFAULT_PROCESS_VERSION = '1.0'
    DEFAULT_PROCESS_DESCRIPTION = 'Description'
    DEFAULT_PROCESS_URI = 'none'

    # properties
    name = models.CharField('name of the process', default=DEFAULT_PROCESS_NAME, max_length=200)
    version = models.CharField('version of the process', default=DEFAULT_PROCESS_VERSION, max_length=15)
    uri = models.CharField('URI', default=DEFAULT_PROCESS_URI, max_length=255)
    description = models.CharField('Description of the process', default=DEFAULT_PROCESS_DESCRIPTION, max_length=255)
    thumbnail = models.CharField(default='', max_length=255, blank=True)
    type =  models.CharField(default='observation', max_length=50, blank=True)

    # relationships
    ownedByAgent = models.ForeignKey(Agent, related_name='processes', null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class Entity(AltaObject):
# class Entity(models.Model):
    # constants
    DEFAULT_TITLE = 'default entity title'

    # properties
    PID = models.CharField('Persistent Identifier.', default=get_defaullt_pid, null=False, max_length=255, unique=True)
    creationTime = models.DateTimeField('Timestamp of creation in the database.', default=datetime.now, blank=True)
    title = models.CharField('Title', default=DEFAULT_TITLE, max_length=200)

    # relationships
    attributedToAgents = models.ManyToManyField(Agent)
    thumbnail = models.CharField(default="/alta-static/images/unknown_entity.jpg", max_length=255, blank=True)

    # release = models.ForeignKey(Release, related_name='released_entities', blank=True, null=True, on_delete=models.SET_NULL)

def __str__(self):
        return str(self.PID)


class TextEntity(Entity):
    text = models.TextField('text', default="")


class Parset(TextEntity):
    pass


class DataEntity(Entity):
    # constants
    DEFAULT_NAME = 'myfile.uvf'

    CHECKSUM_TYPE_MD5 = 'md5'
    CHECKSUM_TYPE_ADLER32 = 'adler32'
    CHECKSUM_TYPE_FLETCHER32 = 'fletcher32'

    CHECKSUM_TYPE_CHOICES = (
        (CHECKSUM_TYPE_MD5, 'md5'),
        (CHECKSUM_TYPE_ADLER32, 'adler32'),
        (CHECKSUM_TYPE_FLETCHER32, 'fletcher32'),
    )

    FORMAT_UNSPECIFIED = 'unspecified'
    FORMAT_MS = 'measurementSet'
    FORMAT_UVFITS = 'uvfits'
    FORMAT_FITS  = 'fits'
    FORMAT_PNG  = 'png'
    FORMAT_PDF  = 'pdf'
    FORMAT_PARSET = 'parset'
    FORMAT_PSRCHIVE = 'psrchive'
    FORMAT_PSRFITS = 'psrfits'
    FORMAT_CHOICES = (
        (FORMAT_UNSPECIFIED, 'unspecified'),
        (FORMAT_MS, 'measurementSet'),
        (FORMAT_UVFITS, 'uvfits'),
        (FORMAT_FITS, 'fits'),
        (FORMAT_PNG, 'png'),
        (FORMAT_PDF, 'pdf'),
        (FORMAT_PARSET, 'parset'),
        (FORMAT_PSRCHIVE, 'psrchive'),
        (FORMAT_PSRFITS, 'psrfits')
    )

    # properties
    name = models.CharField(max_length=200, default=DEFAULT_NAME)
    size = models.BigIntegerField(default=0)
    format = models.CharField('Format', default=FORMAT_MS, max_length=50)
    checksum_type = models.CharField('checksum type (md5, adler32, fletcher32)',
                                     choices=CHECKSUM_TYPE_CHOICES, default=CHECKSUM_TYPE_MD5, max_length=15)
    checksum_value = models.CharField('checksum value', max_length=255)

    class Meta:
        # abstract = True
        ordering = ('creationTime',)

    def __str__(self):
        return self.name


class DataProduct(DataEntity):
    # constants
    TYPE_IMAGE = 'image'
    TYPE_CUBE = 'cube'
    TYPE_PHOTOMETRY = 'photometry'
    TYPE_SPECTRUM = 'spectrum'
    TYPE_SED = 'sed'
    TYPE_TIMESERIES = 'timeSeries'
    TYPE_VISIBILITY = 'visibility'
    TYPE_EVENT = 'event'
    TYPE_CATALOG = 'catalog'
    TYPE_INSPECTIONPLOT = 'inspectionPlot'

    DATAPRODUCT_TYPE_CHOICES = (
        (TYPE_IMAGE, 'image'),
        (TYPE_CUBE, 'cube'),
        (TYPE_PHOTOMETRY, 'photometry'),
        (TYPE_SPECTRUM, 'spectrum'),
        (TYPE_SED, 'sed'),
        (TYPE_TIMESERIES, 'timeseries'),
        (TYPE_VISIBILITY, 'visibility'),
        (TYPE_EVENT, 'event'),
        (TYPE_CATALOG, 'catalog'),
        (TYPE_INSPECTIONPLOT, 'inspectionPlot'),
    )

    SUBTYPE_NONE = ''
    SUBTYPE_LINE_CUBE = 'lineCube'
    SUBTYPE_CONTINUUM_CUBE = 'continuumCube'
    SUBTYPE_POLARIZATION_CUBE = 'polarizationCube'
    SUBTYPE_MOMENT_MAP_IMAGE = 'momentMapImage'
    SUBTYPE_ROTATION_CURVE_IMAGE = 'rotationCurveImage'
    SUBTYPE_PULSAR_TIMING_TIMESERIES = 'pulsarTimingTimeSeries'
    SUBTYPE_PULSAR_SEARCH_TIMESERIES = 'pulsarSearchTimeSeries'
    SUBTYPE_UNCALIBRATED_VISIBILITIES = 'uncalibratedVisibility'
    SUBTYPE_CALTABLE_VISIBILITY = 'calTableVisibility'
    SUBTYPE_CALIBRATED_VISIBILITY = 'calibratedVisibility'
    SUBTYPE_FAST_RADIO_BURST_EVENT = 'fastRadioBurstEvent'
    SUBTYPE_INSPECTIONPLOT = 'inspectionPlot'
    SUBTYPE_INSPECTIONPLOT_BEAMS = 'beams'
    SUBTYPE_INSPECTIONPLOT_AMPLITUDE_BEAMS = 'Amplitude (all)'
    SUBTYPE_INSPECTIONPLOT_AMPLITUDE_TIME = 'Amplitude: f(time)'
    SUBTYPE_INSPECTIONPLOT_AMPLITUDE_ANTENNAS = 'Amplitude: f(antenna)'
    SUBTYPE_INSPECTIONPLOT_AMPLITUDE_BASELINES = 'Amplitude: f(baseline)'
    SUBTYPE_INSPECTIONPLOT_AMPVSCHAN = 'Amplitude vs. channel'
    SUBTYPE_INSPECTIONPLOT_AMPVSTIME = 'Amplitude vs. time'
    SUBTYPE_INSPECTIONPLOT_PHASE_BEAMS = 'Phase (all)'
    SUBTYPE_INSPECTIONPLOT_PHASE_TIME = 'Phase: f(time)'
    SUBTYPE_INSPECTIONPLOT_PHASE_ANTENNAS = 'Phase: f(antenna)'
    SUBTYPE_INSPECTIONPLOT_PHASE_BASELINES = 'Phase: f(baseline)'
    SUBTYPE_INSPECTIONPLOT_PHASEVSCHAN = 'Phase vs. channel'
    SUBTYPE_INSPECTIONPLOT_PHASEVSTIME = 'Phase vs. time'
    SUBTYPE_INSPECTIONPLOT_WATERFALL_AMLPITUDE_AUTOSCALE = 'Waterfall: scaled amplitude'
    SUBTYPE_INSPECTIONPLOT_WATERFALL_AMLPITUDE_NOSCALE = 'Waterfall: unscaled amplitude'
    SUBTYPE_INSPECTIONPLOT_WATERFALL_PHASE_AUTOSCALE = 'Waterfall: scaled phase'
    SUBTYPE_INSPECTIONPLOT_WATERFALL_PHASE_NOSCALE = 'Waterfall: unscaled phase'

    DATAPRODUCT_SUBTYPE_CHOICES = (
        (SUBTYPE_NONE, 'None'),
        (SUBTYPE_LINE_CUBE, 'lineCube'),
        (SUBTYPE_CONTINUUM_CUBE, 'continuumCube'),
        (SUBTYPE_POLARIZATION_CUBE, 'polarizationCube'),
        (SUBTYPE_MOMENT_MAP_IMAGE, 'momentMapImage'),
        (SUBTYPE_ROTATION_CURVE_IMAGE, 'rotationCurveImage'),
        (SUBTYPE_PULSAR_TIMING_TIMESERIES, 'pulsarTimingTimeSeries'),
        (SUBTYPE_PULSAR_SEARCH_TIMESERIES, 'pulsarSearchTimeSeries'),
        (SUBTYPE_UNCALIBRATED_VISIBILITIES, 'uncalibratedVisibility'),
        (SUBTYPE_CALTABLE_VISIBILITY, 'calTableVisibility'),
        (SUBTYPE_CALIBRATED_VISIBILITY, 'calibratedVisibility'),
        (SUBTYPE_FAST_RADIO_BURST_EVENT, 'fastRadioBurstEvent'),
        (SUBTYPE_INSPECTIONPLOT, 'inspectionPlot'),
        (SUBTYPE_INSPECTIONPLOT_AMPLITUDE_BEAMS, 'Amplitude (all)'),
        (SUBTYPE_INSPECTIONPLOT_AMPLITUDE_TIME, 'Amplitude: f(time)'),
        (SUBTYPE_INSPECTIONPLOT_AMPLITUDE_ANTENNAS, 'Amplitude: f(antenna)'),
        (SUBTYPE_INSPECTIONPLOT_AMPLITUDE_BASELINES, 'Amplitude: f(baseline)'),
        (SUBTYPE_INSPECTIONPLOT_AMPVSCHAN, 'Amplitude vs. channel'),
        (SUBTYPE_INSPECTIONPLOT_AMPVSTIME, 'Amplitude vs. time'),
        (SUBTYPE_INSPECTIONPLOT_PHASE_BEAMS, 'Phase (all)'),
        (SUBTYPE_INSPECTIONPLOT_PHASE_TIME, 'Phase: f(time)'),
        (SUBTYPE_INSPECTIONPLOT_PHASE_ANTENNAS, 'Phase: f(antenna)'),
        (SUBTYPE_INSPECTIONPLOT_PHASE_BASELINES, 'Phase: f(baseline)'),
        (SUBTYPE_INSPECTIONPLOT_PHASEVSCHAN, 'Phase vs. channel'),
        (SUBTYPE_INSPECTIONPLOT_PHASEVSTIME, 'Phase vs. time'),
        (SUBTYPE_INSPECTIONPLOT_WATERFALL_AMLPITUDE_AUTOSCALE, 'Waterfall: scaled amplitude'),
        (SUBTYPE_INSPECTIONPLOT_WATERFALL_AMLPITUDE_NOSCALE, 'Waterfall: unscaled amplitude'),
        (SUBTYPE_INSPECTIONPLOT_WATERFALL_PHASE_AUTOSCALE, 'Waterfall: scaled phase'),
        (SUBTYPE_INSPECTIONPLOT_WATERFALL_PHASE_NOSCALE, 'Waterfall: unscaled phase'),
    )

    VO_CALIBRATION_CHOICES = (
        (0, 0),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
    )

    # properties
    dataProductType = models.CharField('DP type (VO compliant)',
                                       default=TYPE_IMAGE, max_length=50)
    dataProductSubType = models.CharField('DP subtype (optional).', max_length=50)
    # relationships
    project = models.ForeignKey(Project, null=True, on_delete=models.SET_NULL)
    contactAgent = models.ForeignKey(Agent, related_name='%(class)s_contact_agent', null=True,
                                     on_delete=models.SET_NULL)
    contributingAgents = models.ManyToManyField(Agent, blank=True)

    calibrationLevel =  models.IntegerField('calibrationLevel', choices=VO_CALIBRATION_CHOICES, default=0)

    RA = models.FloatField('RA', null = True)
    dec = models.FloatField('dec', null = True)
    fov = models.FloatField('fov', null = True)
    equinox = models.CharField('equinox', default='J2000', max_length=20, null = True)

    rights = models.CharField('rights', default=RIGHTS_PUBLIC, max_length=15)
    storageRef = models.CharField('storageRef', default="", max_length=255)

    releaseDate = models.DateTimeField('release date', default=datetime.now)
    contentDescription = models.CharField('content description', default="", max_length=255, blank=True)
    datasetID = models.CharField('datasetID', default="", max_length=255, blank=True)

    # possible values: archive, cold_disk, cold_tape
    locality = models.CharField('locality', default="archive", max_length=50)

    marked_for_deletion = models.BooleanField('marked_for_deletion', default=False)

    release = models.ForeignKey(Release, related_name='released_entities', blank=True, null=True, on_delete=models.SET_NULL)

    @property
    def derived_project_name(self):
        return self.project.name

    @property
    def derived_release_id(self):
        # isolate the first taskid (in case of more), good enough to determine the release
        try:
            runid = self.datasetID.split('_')[0]

            # find object with 'datasetID'
            ancestor_observation = Observation.objects.get(datasetID=runid)
            # isolate the first tasid (in case of more)

            release_id = ancestor_observation.release.release_id

        except:
            # a failure to retrieve a release_id should not crash the whole backend, so continue
            return None
        return release_id

    # print returns this
    def __str__(self):
        s = str(self.PID)
        return s

    class Meta:
        #ordering = ('creationTime',)
        ordering = ('PID',)


class Activity(AltaObject):

    # properties
    DEFAULT_ACTIVITY_RUNID = 0
    runId = models.CharField('runId', max_length=200, default=DEFAULT_ACTIVITY_RUNID, unique=True)
    startTime = models.DateTimeField('start time', default=datetime.now)
    endTime = models.DateTimeField('end time', default=datetime.now)

    # relationships
    process = models.ForeignKey(Process, null=True, on_delete=models.SET_NULL, blank=False)
    associatedWithAgent = models.ForeignKey(Agent, null=True, on_delete=models.SET_NULL)

    # multiple relationships with Entity, distinguish with the 'related_name' keyword
    parameterEntities = models.ManyToManyField(Entity, related_name='parameterForActivity', blank=True)
    usedEntities = models.ManyToManyField(Entity, related_name='usedByActivity', blank=True)
    generatedEntities = models.ManyToManyField(Entity, related_name='generatedByActivity', blank=True)

    thumbnail = models.CharField(default="https://alta.astron.nl/alta-static/images/unknown_activity.jpg", max_length=255, blank=True)
    datasetID = models.CharField('datasetID', default="", max_length=255, blank=True)

    project = models.ForeignKey(Project, null=True, on_delete=models.SET_NULL)
    rights = models.CharField('rights', default=RIGHTS_PUBLIC, max_length=15, blank=True)

    # in minutes : default = 60 days
    # remove this field, this is going to be specified in ATDB and convert by the ingest to 'scrub_timestamp'
    max_lifetime_on_disk = models.BigIntegerField(default=86400)

    default_scrub_datetimestamp = datetime.now() + timedelta(days=60)
    formatted_timestamp = default_scrub_datetimestamp.strftime(DJANGO_TIME_FORMAT)
    scrub_timestamp = models.DateTimeField('scrub_timestamp', default=formatted_timestamp, blank=True, null=True)

    # possible values: archive, cold_disk, cold_tape
    locality_policy = models.CharField('locality_policy', default="cold_tape", max_length=50)
    # possible values:
    locality_status = models.CharField('locality_status', default="inarchive", max_length=50)

    # Quality
    quality = models.CharField(max_length=30, default="unknown")
    release = models.ForeignKey(Release, related_name='released_activities', blank=True, null=True, on_delete=models.SET_NULL)

    @property
    def derived_project_name(self):
        return self.project.name

    @property
    def derived_process_type(self):
        try:
            process_type = self.process.type
        except:
            process_type = 'observation'
        return process_type

    @property
    def derived_process_uri(self):
        try:
            uri = self.process.uri
        except:
            uri = 'unknown'
        return uri

    @property
    def derived_process_thumbnail(self):
        return self.process.thumbnail

    @property
    def derived_duration(self):
        # calculate the duration in hours
        delta_seconds = (self.endTime - self.startTime)
        delta_hours = delta_seconds / 3600
        return delta_hours

    @property
    def derived_release_id(self):
        return self.release.release_id

    def __str__(self):
        return str(self.runId)

    class Meta:
        # abstract = True
        ordering = ('-runId',)


class ReleaseActivity(Activity):
    """
    # ReleaseActivity is a subclass of Activity that is saved only to update release information.
    # distinguish between Activity and ReleaseActivity makes it possible to have different behaviour in the
    $ post_save_activity_handler signal handlers
    # (it is used and saved in the algorithms.mark_as_release() function for that purpose.
    """
    queryset = Activity.objects.all()

    class Meta:
        proxy = True


class Observation(Activity):
    target = models.CharField('target', max_length=80, default="no target")
    configuration = models.CharField('configuration', max_length=80, default="no configuration")
    facility = models.CharField('facility', max_length=80, default="no facility")
    instrument = models.CharField('instrument', max_length=80, default="no instrument")
    log = models.CharField('log', max_length=254, default="no log")

    success = models.CharField('success', default="?", max_length=5)
    RA = models.FloatField('RA',null=True)
    dec = models.FloatField('Dec', null=True)
    fov = models.FloatField('fov', null=True)
    equinox = models.CharField('equinox', default='J2000', max_length=6, null=True)
    lo = models.FloatField('LO', null=True)
    sub1 = models.FloatField('Sub1',null=True)
    bandwidth = models.FloatField('bandwidth',null=True)
    antennaset = models.CharField('antennaset', max_length=80, default="unknown")
    scheduler = models.CharField('scheduler', max_length=15, default="unknown")
    observer = models.CharField('observer', max_length=15, default="unknown")
    comment = models.CharField('comment', max_length=254, default="", blank=True)

    calibrator_target = models.CharField(default="unknown", max_length=15, blank=True)
    calibrator_sources = models.CharField('cal_sources', default="", max_length=255, blank=True)
    calibrator_runIds = models.CharField('cal_runids', default="", max_length=255, blank=True)

    def __str__(self):
        return str(self.runId)

    @property
    def derived_process_uri(self):
        try:
            uri = self.process.uri
        except:
            uri = 'unknown'
        return uri

# only retrieve the RA, dec fields (for better performance)
class ObservationCoordinatesManager(models.Manager):
    def get_queryset(self):
        # logger.info("ObservationCoordinatesManager.get_queryset()")
        return super(ObservationCoordinatesManager, self).get_queryset().only('RA','dec','fov')


# this is a proxy model of Observation that only uses RA, Dec and a derived longitude field to serve WebGL Globe.
class ObservationCoordinates(Observation):
    objects = ObservationCoordinatesManager()

    class Meta:
        proxy = True


# this is a proxy model of Observation that only uses RA, Dec and a derived longitude field to serve WebGL Globe.
class ObservationCoordinatesGlobe(Observation):
    objects = ObservationCoordinatesManager()

    @property
    def derived_longitude(self):
        longitude = self.RA
        if longitude<0:
           longitude += 360
        longitude = 360 - longitude
        return (longitude)

    class Meta:
        proxy = True

# file upload (for thumbnail images to the alta-static website)
def my_directory_path(instance, filename):
    """
    overrides the location where the file is written and adds the provided directory to it.
    :param instance:
    :param filename:
    :return:
    """
    my_file = os.path.join(instance.directory, filename)
    return my_file


# Files to upload (images and inspectionplots)
class AltaFile(models.Model):
  # using a custom 'storage' to overwrite existing files.
  # (because the 'delete' methods do not delete associated images).
  file = models.FileField(upload_to=my_directory_path, storage=OverwriteStorage(), blank=False, null=False)

  description = models.CharField(max_length=128, blank=True)
  directory = models.CharField(max_length=128, blank=True)
  timestamp = models.DateTimeField(auto_now_add=True)

  def __str__(self):
      return self.file.name