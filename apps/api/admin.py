from django.contrib import admin
from .models import Ingest, AltaObject, Agent, Process, Activity, Observation, Project, Entity, DataEntity, TextEntity, Parset, DataProduct, Release

# Register your models here.
admin.site.register(Ingest)
admin.site.register(AltaObject)
admin.site.register(Agent)
admin.site.register(Process)
admin.site.register(Activity)
admin.site.register(Observation)
admin.site.register(Project)
admin.site.register(Release)

admin.site.register(Entity)
admin.site.register(DataEntity)
admin.site.register(TextEntity)
admin.site.register(Parset)
admin.site.register(DataProduct)