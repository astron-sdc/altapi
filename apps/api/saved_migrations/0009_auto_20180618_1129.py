# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-06-18 09:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20180612_0851'),
    ]

    operations = [
        migrations.CreateModel(
            name='ObservationCoordinates',
            fields=[
            ],
            options={
                'indexes': [],
                'proxy': True,
            },
            bases=('api.observation',),
        ),
        migrations.AddField(
            model_name='activity',
            name='project',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.Project'),
        ),
        migrations.AddField(
            model_name='activity',
            name='rights',
            field=models.CharField(blank=True, default='public', max_length=15, verbose_name='rights'),
        ),
        migrations.AddField(
            model_name='observation',
            name='fov',
            field=models.FloatField(null=True, verbose_name='fov'),
        ),
    ]
