# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-08-21 13:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_auto_20180806_1322'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dataproduct',
            options={'ordering': ('PID',)},
        ),
        migrations.AlterField(
            model_name='observation',
            name='lo',
            field=models.FloatField(null=True, verbose_name='LO'),
        ),
        migrations.AlterField(
            model_name='observation',
            name='sub1',
            field=models.FloatField(null=True, verbose_name='Sub1'),
        ),
    ]
