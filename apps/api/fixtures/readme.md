fixtures
========
See docs: https://docs.djangoproject.com/en/1.10/howto/initial-data/


mydatabase.sqlite ==> api.json:
> python manage.py dumpdata api --settings=altapi.settings.dev --indent=4 > apps/api/fixtures/api_relational.json

api.json ==> mydatabase.sqlite:
> python manage.py loaddata apps/api/fixtures/api.json --settings=altapi.settings.dev