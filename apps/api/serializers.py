from rest_framework import serializers

from .models import Ingest, AltaObject
from .models import Agent, Process, Activity, Project, Observation, ObservationCoordinates, Release
from .models import DataEntity, TextEntity
from .models import DataProduct, Parset
from .models import Entity, AltaFile

from django.contrib.auth.models import User

# Serializers define the API representation.

# http://localhost:8000/altapi/ingests
class IngestSerializer(serializers.HyperlinkedModelSerializer):
    ingested = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=AltaObject.objects.all(),
        view_name='altaobject-detail-view',
        required=False,
        lookup_field='pk'
    )
    nr_dataproducts = serializers.SerializerMethodField()
    # ingested_observations = serializers.SerializerMethodField()
    # ingested_activities = serializers.SerializerMethodField()

    obs_runid = serializers.SerializerMethodField()
    act_runid = serializers.SerializerMethodField()
    act_datasetid = serializers.SerializerMethodField()

    class Meta:
        model = Ingest
        # fields = '__all__'
        fields = ('id','PID', 'status', 'timestamp', 'username', 'ingested',
                  'nr_dataproducts', 'obs_runid', 'act_runid', 'act_datasetid')

    def get_nr_dataproducts(self, instance):
        count = AltaObject.objects.filter(type='dataproduct', ingest_id=instance.id).count()
        return count

    def get_obs_runid(self, instance):
        try:
            observation_object = AltaObject.objects.filter(type='observation', ingest_id=instance.id)[0]
            observation = Observation.objects.get(id=observation_object.id)
            return observation.runId
        except:
            return -1

    def get_act_runid(self, instance):
        try:
            activity_object = AltaObject.objects.filter(type='activity', ingest_id=instance.id)[0]
            activity =Activity.objects.get(id=activity_object.id)
            return activity.runId
        except:
            return -1

    def get_act_datasetid(self, instance):
        try:
            activity_object = AltaObject.objects.filter(type='activity', ingest_id=instance.id)[0]
            activity =Activity.objects.get(id=activity_object.id)
            return activity.datasetID
        except:
            return -1


class IngestFlatSerializer(serializers.HyperlinkedModelSerializer):
    ingested = serializers.StringRelatedField(
        many=True,
        required=False,
    )
    nr_dataproducts = serializers.SerializerMethodField()
    # ingested_observations = serializers.SerializerMethodField()
    # ingested_activities = serializers.SerializerMethodField()

    obs_runid = serializers.SerializerMethodField()
    act_runid = serializers.SerializerMethodField()
    act_datasetid = serializers.SerializerMethodField()

    class Meta:
        model = Ingest
        # fields = '__all__'
        fields = ('id', 'PID', 'status', 'timestamp', 'username', 'ingested',
                  'nr_dataproducts', 'obs_runid', 'act_runid', 'act_datasetid')

    def get_nr_dataproducts(self, instance):
        count = AltaObject.objects.filter(type='dataproduct', ingest_id=instance.id).count()
        return count

    def get_obs_runid(self, instance):
        try:
            observation_object = AltaObject.objects.filter(type='observation', ingest_id=instance.id)[0]
            observation = Observation.objects.get(id=observation_object.id)
            return observation.runId
        except:
            return -1

    def get_act_runid(self, instance):
        try:
            activity_object = AltaObject.objects.filter(type='activity', ingest_id=instance.id)[0]
            activity = Activity.objects.get(id=activity_object.id)
            return activity.runId
        except:
            return -1

    def get_act_datasetid(self, instance):
        try:
            activity_object = AltaObject.objects.filter(type='activity', ingest_id=instance.id)[0]
            activity = Activity.objects.get(id=activity_object.id)
            return activity.datasetID
        except:
            return -1


# http://localhost:8000/altapi/ingests
class AltaObjectSerializer(serializers.HyperlinkedModelSerializer):

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    class Meta:
        model = AltaObject
        #fields = '__all__'
        fields = ('id','type','ingest')


# http://localhost:8000/altapi/agents
class AgentSerializer(serializers.HyperlinkedModelSerializer):
    # this adds a 'processes' list with hyperlinks to the Agent API.
    # note that 'processes' is not defined in the Agent model, but in the process model.
    processes = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Process.objects.all(),
        view_name='process-detail-view',
        required=False,
        lookup_field='pk'
    )

    class Meta:
        model = Agent
        # fields = '__all__'
        fields = ('id', 'name', 'externalRef', 'processes')


# http://localhost:8000/altapi/processes/1/
class ProcessSerializer(serializers.HyperlinkedModelSerializer):
    # this is how a related field is represented as a hyperlink
    ownedByAgent = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk'
    )

    class Meta:
        model = Process
        # fields = '__all__'
        fields = ('id', 'name', 'version', 'uri', 'description', 'ownedByAgent','thumbnail','type')


class ProcessTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Process
        # fields = '__all__'
        fields = ('id', 'type')

class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project
        # fields = '__all__'
        fields = ('id', 'name', 'externalRef', 'rights')


class EntitySerializer(serializers.HyperlinkedModelSerializer):
    generatedByActivity = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Activity.objects.all(),
        view_name='activity-detail-view',
        required=False,
        lookup_field='pk'
    )
    usedByActivity = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Activity.objects.all(),
        view_name='activity-detail-view',
        required=False,
        lookup_field='pk'
    )
    parameterForActivity = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Activity.objects.all(),
        view_name='activity-detail-view',
        required=False,
        lookup_field='pk'
    )
    attributedToAgents = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    release = serializers.HyperlinkedRelatedField(
        many=False,
        required=False,
        allow_null=True,
        queryset = Release.objects.all(),
        view_name = 'release-detail-view',
        lookup_field = 'pk'
    )

    class Meta:
        model = Entity
        fields = ('id', 'PID', 'title', 'creationTime', 'attributedToAgents',
                  'generatedByActivity','usedByActivity','parameterForActivity','ingest','release')


# note how 'PID', 'title' and 'creationTime' are returned, they are inherited from Entity
class DataEntitySerializer(serializers.HyperlinkedModelSerializer):

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    class Meta:
        model = DataEntity
        fields = ('id', 'PID','title', 'creationTime', 'name', 'size', 'format', 'checksum_type', 'checksum_value', 'ingest')

class TextEntitySerializer(serializers.HyperlinkedModelSerializer):

    attributedToAgents = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    class Meta:
        model = TextEntity
        fields = ('id', 'PID','title', 'creationTime', 'text','attributedToAgents', 'ingest')

class ParsetSerializer(serializers.HyperlinkedModelSerializer):

    attributedToAgents = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    class Meta:
        model = Parset
        fields = ('id', 'PID','title', 'creationTime', 'text', 'attributedToAgents', 'ingest')


class DataProductSerializer(serializers.HyperlinkedModelSerializer):

    project = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Project.objects.all(),
        view_name='project-detail-view',
        lookup_field='pk',
        required=True
    )

    contactAgent = serializers.HyperlinkedRelatedField(
        label='Contact Agent',
        many=False,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=True
    )

    contributingAgents = serializers.HyperlinkedRelatedField(
        label='Contributing Agents',
        many=True,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    attributedToAgents = serializers.HyperlinkedRelatedField(
        label='Attributed To Agents',
        many=True,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    generatedByActivity = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Activity.objects.all(),
        view_name='activity-detail-view',
        required=False,
        lookup_field='pk'
    )

    usedByActivity = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Activity.objects.all(),
        view_name='activity-detail-view',
        required=False,
        lookup_field='pk'
    )

    parameterForActivity = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Activity.objects.all(),
        view_name='activity-detail-view',
        required=False,
        lookup_field='pk'
    )

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    release = serializers.HyperlinkedRelatedField(
        many=False,
        required=False,
        allow_null=True,
        queryset = Release.objects.all(),
        view_name = 'release-detail-view',
        lookup_field = 'pk'
    )

    class Meta:
        model = DataProduct
        fields = ('id', 'PID', 'dataProductType', 'dataProductSubType', 'title', 'creationTime',
                  'name', 'size', 'format', 'checksum_type', 'checksum_value',
                  'calibrationLevel','storageRef','datasetID','rights','releaseDate','contentDescription',
                  'RA', 'dec', 'fov',
                  'project', 'derived_project_name','contactAgent', 'contributingAgents', 'attributedToAgents',
                  'generatedByActivity', 'usedByActivity', 'parameterForActivity', 'ingest', 'thumbnail',
                  'locality','derived_release_id','release','calibrationLevel')


class DataProductFlatSerializer(serializers.ModelSerializer):

    project = serializers.StringRelatedField(
        many=False,
        required=False
    )

    contactAgent = serializers.StringRelatedField(
        many=False,
        required=False
    )

    contributingAgents = serializers.StringRelatedField(
        many=True,
        required=False
    )

    attributedToAgents = serializers.StringRelatedField(
        many=True,
        required=False
    )

    generatedByActivity = serializers.StringRelatedField(
        many=True,
        required=False,
        read_only=True
        # keep this read_only, otherwise an accidental empty update in the REST API will sever the relationship
    )

    usedByActivity = serializers.StringRelatedField(
        many=True,
        required=False,
        read_only=True
    )

    parameterForActivity = serializers.StringRelatedField(
        many=True,
        required=False,
        read_only=True
    )

    ingest = serializers.StringRelatedField(
        many=False,
        required=False
    )

    release = serializers.HyperlinkedRelatedField(
        many=False,
        required=False,
        allow_null=True,
        queryset = Release.objects.all(),
        view_name = 'release-detail-view',
        lookup_field = 'pk'
    )

    class Meta:
        model = DataProduct
        fields = ('id', 'PID', 'dataProductType', 'dataProductSubType', 'title', 'creationTime',
                  'name', 'size', 'format', 'checksum_type', 'checksum_value',
                  'calibrationLevel','storageRef','datasetID','rights','releaseDate','contentDescription',
                  'RA', 'dec', 'fov',
                  'project', 'derived_project_name','contactAgent', 'contributingAgents', 'attributedToAgents',
                  'generatedByActivity', 'usedByActivity', 'parameterForActivity', 'ingest', 'thumbnail',
                  'locality','marked_for_deletion','derived_release_id','release','calibrationLevel')


class ActivitySerializer(serializers.HyperlinkedModelSerializer):

    project = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Project.objects.all(),
        view_name='project-detail-view',
        lookup_field='pk',
        required=False
    )

    process = serializers.HyperlinkedRelatedField(
       many=False,
       queryset=Process.objects.all(),
       view_name='process-detail-view',
       lookup_field='pk',
       required=False
    )


    associatedWithAgent = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    parameterEntities = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Entity.objects.all(),
        view_name='entity-detail-view',
        lookup_field='pk',
        required=False
    )

    usedEntities = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Entity.objects.all(),
        view_name='entity-detail-view',
        lookup_field='pk',
        required=False
    )

    generatedEntities = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Entity.objects.all(),
        view_name='entity-detail-view',
        lookup_field='pk',
        required=False
    )

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )


    class Meta:
        model = Activity
        # fields = '__all__'
        fields = ('id', 'runId', 'startTime', 'endTime', 'derived_duration', 'project','derived_project_name',
                  'process', 'rights','associatedWithAgent','derived_release_id',
                  'parameterEntities', 'usedEntities', 'generatedEntities', 'ingest',
                  'thumbnail','datasetID', 'derived_process_type', 'derived_process_thumbnail',
                  'scrub_timestamp','locality_policy','locality_status','quality', 'derived_process_uri')


class ActivityFlatSerializer(serializers.HyperlinkedModelSerializer):

    project = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Project.objects.all(),
        view_name='project-detail-view',
        lookup_field='pk',
        required=False
    )

    process = serializers.HyperlinkedRelatedField(
       many=False,
       queryset=Process.objects.all(),
       view_name='process-detail-view',
       lookup_field='pk',
       required=False
    )


    associatedWithAgent = serializers.StringRelatedField(
        many=False,
        required=False
    )

    parameterEntities = serializers.StringRelatedField(
        many=False,
        required=False
    )

    usedEntities = serializers.StringRelatedField(
        many=False,
        required=False
    )

    generatedEntities = serializers.StringRelatedField(
        many=False,
        required=False
    )

    ingest= serializers.StringRelatedField(
        many=False,
        required=False
    )

    release = serializers.HyperlinkedRelatedField(
        many=False,
        required=False,
        allow_null=True,
        queryset = Release.objects.all(),
        view_name = 'release-detail-view',
        lookup_field = 'pk'
    )

    class Meta:
        model = Activity
        # fields = '__all__'
        fields = ('id', 'runId', 'startTime', 'endTime', 'derived_duration', 'project','derived_project_name',
                  'process', 'rights','associatedWithAgent','derived_release_id',
                  'parameterEntities', 'usedEntities', 'generatedEntities', 'ingest',
                  'thumbnail','datasetID', 'derived_process_type', 'derived_process_thumbnail',
                  'scrub_timestamp','locality_policy','locality_status','quality','derived_process_uri','release')


class ObservationSerializer(serializers.HyperlinkedModelSerializer):
    project = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Project.objects.all(),
        view_name='project-detail-view',
        lookup_field='pk',
        required=False
    )
    process = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Process.objects.all(),
        view_name='process-detail-view',
        lookup_field='pk',
        required = False
    )
    associatedWithAgent = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required = False
    )
    parameterEntities = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Entity.objects.all(),
        view_name='entity-detail-view',
        lookup_field='pk',
        required=False
    )
    usedEntities = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Entity.objects.all(),
        view_name='entity-detail-view',
        lookup_field='pk',
        required=False
    )
    generatedEntities = serializers.HyperlinkedRelatedField(
        many=True,
        # read_only=True,
        queryset=Entity.objects.all(),
        view_name='entity-detail-view',
        lookup_field='pk',
        required=False
    )

    ingest = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Ingest.objects.all(),
        view_name='ingest-detail-view',
        lookup_field='pk',
        required=False
    )

    #startTime = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    #endTime = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')

    class Meta:
        model = Observation
        # fields = '__all__'
        fields = ('id', 'target','runId', 'startTime', 'endTime', 'derived_duration','project','derived_project_name',
                  'rights','process', 'derived_process_type','associatedWithAgent',
                  'target','configuration','facility','instrument','log',
                  'parameterEntities', 'usedEntities', 'generatedEntities', 'ingest', 'thumbnail','datasetID',
                  'success','RA','dec','fov','equinox','lo','sub1','bandwidth','antennaset','observer','scheduler','comment',
                  'calibrator_target','calibrator_sources', 'calibrator_runIds','derived_release_id',
                  'scrub_timestamp', 'locality_policy', 'locality_status', 'quality', 'comment', 'derived_process_uri')


class ObservationFlatSerializer(serializers.ModelSerializer):
#    project = serializers.StringRelatedField(
#        many=False,
#        required=False
#    )
#    process = serializers.StringRelatedField(
#        many=False,
#        required = False
#    )
#    associatedWithAgent = serializers.StringRelatedField(
#        many=False,
#        required = False
#    )

    project = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Project.objects.all(),
        view_name='project-detail-view',
        lookup_field='pk',
        required=False
    )
    process = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Process.objects.all(),
        view_name='process-detail-view',
        lookup_field='pk',
        required=False
    )
    associatedWithAgent = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk',
        required=False
    )

    ingest = serializers.StringRelatedField(
        many=False,
        required=False,
        read_only=True,
    )


    usedEntities = serializers.StringRelatedField(
        many=True,
        required=False,
        read_only=True,
    )
    generatedEntities = serializers.StringRelatedField(
        many=True,
        required=False,
        read_only=True,
    )


    release = serializers.HyperlinkedRelatedField(
        many=False,
        required=False,
        allow_null=True,
        queryset=Release.objects.all(),
        view_name='release-detail-view',
        lookup_field='pk'
    )

    class Meta:
        model = Observation
        # fields = '__all__'
        fields = ('id', 'target','runId', 'startTime', 'endTime', 'derived_duration','project','derived_project_name',
                  'rights','process', 'derived_process_type','associatedWithAgent',
                  'target','configuration','facility','instrument','log',
                  'ingest', 'thumbnail','datasetID','usedEntities','generatedEntities',
                  'success','RA','dec','fov','equinox','lo','sub1','bandwidth','antennaset','observer','scheduler','comment',
                  'calibrator_target','calibrator_sources', 'calibrator_runIds','derived_release_id',
                  'scrub_timestamp','locality_policy','locality_status','quality', 'release','comment')

class ObservationCoordinatesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ObservationCoordinates
        fields = ('runId','target','RA','dec','fov')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


# Serializer for file uploads
class AltaFileSerializer(serializers.ModelSerializer):
    class Meta():
        model = AltaFile
        fields = "__all__"


class ReleaseSerializer(serializers.ModelSerializer):

    agent = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Agent.objects.all(),
        view_name='agent-detail-view',
        lookup_field='pk'
    )

    project = serializers.HyperlinkedRelatedField(
        many=False,
        # read_only=True,
        queryset=Project.objects.all(),
        view_name='project-detail-view',
        lookup_field='pk',
        required=True
    )

    # this adds a 'released_activities' list with hyperlinks to the Agent API.
    # note that 'released_activities' is not defined in the Releaes model, but in the Activities model.
#    released_activities = serializers.StringRelatedField(
#        many=True,
#        required=False,
#        read_only=True
#    )

    nr_released_activities = serializers.SerializerMethodField()
    nr_released_observations = serializers.SerializerMethodField()

    class Meta:
        model = Release
        fields = ('id', 'release_id', 'name', 'releaseDate', 'documentation','agent','project','rights',
                  'nr_released_activities','nr_released_observations','status')

    def get_nr_released_activities(self, instance):
        count = Activity.objects.filter(release_id=instance.id).count()
        return count

    def get_nr_released_observations(self, instance):
        count = Activity.objects.filter(type='observation', release_id=instance.id).count()
        return count

class GetStorageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Activity
        fields = ('runId',)