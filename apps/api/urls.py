from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.views import obtain_auth_token
# app imports
from . import views
from django.urls import path

urlpatterns = [
    url(r'^altapi/ingests$', views.IngestListView.as_view(), name='ingest-list-view'),
    url(r'^altapi/ingests/(?P<pk>[0-9]+)/$', views.IngestDetailView.as_view(), name='ingest-detail-view'),
    url(r'^altapi/ingests-flat$', views.IngestFlatListView.as_view(), name='ingest-flat-list-view'),
    url(r'^altapi/ingests-flat/(?P<pk>[0-9]+)/$', views.IngestFlatDetailView.as_view(), name='ingest-flat-detail-view'),

    url(r'^altapi/altaobjects$', views.AltaObjectListView.as_view(), name='altaobject-list-view'),
    url(r'^altapi/altaobjects/(?P<pk>[0-9]+)/$', views.AltaObjectDetailView.as_view(), name='altaobject-detail-view'),

    url(r'^altapi/projects$', views.ProjectListView.as_view(), name='project-list-view'),
    url(r'^altapi/projects/(?P<pk>[0-9]+)/$', views.ProjectDetailView.as_view(), name='project-detail-view'),

    url(r'^altapi/agents$', views.AgentListView.as_view(), name='agent-list-view'),
    url(r'^altapi/agents/(?P<pk>[0-9]+)/$', views.AgentDetailView.as_view(), name='agent-detail-view'),

    url(r'^altapi/processes$', views.ProcessListView.as_view(), name='process-list-view'),
    url(r'^altapi/processes/(?P<pk>[0-9]+)/$', views.ProcessDetailView.as_view(), name='process-detail-view'),

    url(r'^altapi/activities$', views.ActivityListView.as_view(), name='activity-list-view'),
    url(r'^altapi/activities/(?P<pk>[0-9]+)/$', views.ActivityDetailView.as_view(), name='activity-detail-view'),

    # this shows the same view as the activities views, except that the hyperlink serializers have been
    # replaced with stringfield serializers, which makes them very very much faster.
    url(r'^altapi/activities-flat$', views.ActivityFlatListView.as_view(), name='activity-flat-list-view'),
    url(r'^altapi/activities-flat/(?P<pk>[0-9]+)/$', views.ActivityFlatDetailView.as_view(), name='activity-flat-detail-view'),

    url(r'^altapi/observations$', views.ObservationListView.as_view(), name='observation-list-view'),
    url(r'^altapi/observations/(?P<pk>[0-9]+)/$', views.ObservationDetailView.as_view(),name='observation-detail-view'),

    # this shows the same view as the observations views, except that the hyperlink serializers have been
    # replaced with stringfield serializers, which makes them very very much faster.
    url(r'^altapi/observations-flat$', views.ObservationFlatListView.as_view(), name='observation-flat-list-view'),
    url(r'^altapi/observations-flat/(?P<pk>[0-9]+)/$', views.ObservationFlatDetailView.as_view(),name='observation-flat-detail-view'),

    url(r'^altapi/pipelines$', views.PipelineListView.as_view(), name='pipeline-list-view'),
    url(r'^altapi/pipelines-flat$', views.PipelineFlatListView.as_view(), name='pipeline-flat-list-view'),

    url(r'^altapi/entities$', views.EntityListView.as_view(), name='entity-list-view'),
    url(r'^altapi/entities/(?P<pk>[0-9]+)/$', views.EntityDetailView.as_view(), name='entity-detail-view'),

    url(r'^altapi/dataentities$', views.DataEntityListView.as_view(), name='dataentity-list-view'),
    url(r'^altapi/dataentities/(?P<pk>[0-9]+)/$', views.DataEntityDetailsView.as_view(), name='dataentity-detail-view'),

    url(r'^altapi/dataproducts$', views.DataProductListView.as_view()),
    url(r'^altapi/dataproducts/(?P<pk>[0-9]+)/$', views.DataProductDetailView.as_view()),
    url(r'^altapi/dataproducts-flat$', views.DataProductFlatListView.as_view()),
    url(r'^altapi/dataproducts-flat/(?P<pk>[0-9]+)/$', views.DataProductFlatDetailView.as_view()),

    url(r'^altapi/textentities$', views.TextEntityListView.as_view()),
    url(r'^altapi/textentities/(?P<pk>[0-9]+)/$', views.TextEntityDetailsView.as_view(), name='textentity-detail-view'),

    url(r'^altapi/parsets$', views.ParsetListView.as_view()),
    url(r'^altapi/parsets/(?P<pk>[0-9]+)/$', views.ParsetDetailsView.as_view(), name='parset-detail-view'),

    url(r'^altapi/obs-coords$', views.ObservationCoordinatesGlobeListView.as_view()),
    url(r'^altapi/observation-coordinates$', views.ObservationCoordinatesListView.as_view()),

    url(r'^altapi/users$', views.UserListView.as_view()),
    url(r'^altapi/users/(?P<pk>[0-9]+)/$', views.UserDetailView.as_view()),

#    url(r'^altapi/users', views.UserViewSet),
    url(r'^altapi/obtain-auth-token/$', csrf_exempt(obtain_auth_token)),

#    this publishes the API, but not everything is behind authentication, so better to have some security through obscurity first
#    url('^altapi/schema.json$', views.schema_view),

# upload a file to the media directory (for uploading thumbnails to the webserver
    url(r'^altapi/upload/$', views.AltaFileView.as_view(), name='file-upload'),
    url(r'^altapi/uploads$', views.UploadsView.as_view()),
    url(r'^altapi/uploads/(?P<pk>[0-9]+)/$', views.UploadsDetailsView.as_view()),

    url(r'^altapi/releases$', views.ReleaseListView.as_view()),
    url(r'^altapi/releases/(?P<pk>[0-9]+)/$', views.ReleaseDetailView.as_view(), name='release-detail-view'),

# (cold) storage functionality
    path('altapi/get-storage', views.GetStorageView.as_view(), name='get-storage'),

    # https://alta.astron.nl/altapi/get-freeze
    path('altapi/get-freeze', views.GetFreezeView.as_view(), name='get-freeze'),

    # https://alta.astron.nl/altapi/get-scrub
    path('altapi/get-scrub', views.GetScrubView.as_view(), name='get-scrub'),

    # https://alta.astron.nl/altapi/get-scrub
    path('altapi/get-prepared-coldstorage', views.GetPreparedColdStorageView.as_view(), name='get-prepared-coldstorage'),

     # https://alta.astron.nl/altapi/get-grune
    path('altapi/get-prune', views.GetPruneView.as_view(), name='get-prune'),

    # https://alta.astron.nl/altapi/get-marked-for-deletion
    path('altapi/get-marked-for-deletion', views.GetMarkedForDeletionView.as_view(), name='get-marked-for-deletion'),

    # first generation locality setter
    # https://alta.astron.nl/altapi/put-locality?taskid=190812001&locality=inarchive
    path('altapi/put-locality', views.PutLocalityView.as_view(), name='put-locality'),

    # second generation locality setters that append localities instead of replacing them
    # https://alta.astron.nl/altapi/set-locality?taskid=190812001&locality=inarchive
    path('altapi/append-locality', views.AppendLocalityView.as_view(), name='append-locality'),
    path('altapi/remove-locality', views.RemoveLocalityView.as_view(), name='remove-locality'),

    path('altapi/coldstorage', views.ColdStorageView.as_view(), name='cold-storage'),

    # release functionality
    # https://alta.astron.nl/altapi/mark-as-release?taskid_from=190801001&taskid_to=190901001&release_id=R1_190813
    path('altapi/mark-as-release', views.MarkAsReleaseView.as_view(), name='mark-as-release'),

    # https://alta.astron.nl/altapi/get-final-releases
    path('altapi/get-final-releases', views.GetFinalReleasesView.as_view(), name='get-final-releases'),

    # https://alta.astron.nl/altapi/put-release-status?release_id=early_science&status=published
    path('altapi/put-release-status', views.PutReleaseStatusView.as_view(), name='put-release-status'),

    # other functions
    # https://alta.astron.nl/altapi/get-activity-details?runid=190812001
    path('altapi/get-activity-details', views.GetActivityDetails.as_view(), name='get-activity-details'),

    # https://alta.astron.nl/altapi/get-dataproducts-types?runid=190812001
    path('altapi/get-dataproducts-types', views.GetDataProductsDetails.as_view(), name='get-dataproducts-types'),

    # https://alta.astron.nl/altapi/append-storageref?PID=pid-200427001-039-3C147.G0ph&storageref=cold:200427001_AP_B039.TAR
    path('altapi/append-storageref', views.AppendStorageRefView.as_view(), name='append-storageref'),
    # https://alta.astron.nl/altapi/remove-storageref?PID=pid-200427001-039-3C147.G0ph&storageref=cold:200427001_AP_B039.TAR
    path('altapi/remove-storageref', views.RemoveStorageRefView.as_view(), name='remove-storageref'),

    # https://alta.astron.nl/altapi/append-storageref-byname?runid=200427001&name=200427001_AP_B039.TAR&storageref=cold:200427001_AP_B039.TAR
    path('altapi/append-storageref-byname', views.AppendStorageRefByNameView.as_view(), name='append-storageref-byname'),
    # https://alta.astron.nl/altapi/remove-storageref-byname?runid=200427001&name=200427001_AP_B039.TAR&storageref=cold:200427001_AP_B039.TAR
    path('altapi/remove-storageref-byname', views.RemoveStorageRefByNameView.as_view(), name='remove-storageref-byname'),


]

urlpatterns = format_suffix_patterns(urlpatterns)