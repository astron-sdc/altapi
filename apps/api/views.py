import json
import django_filters
import logging, pdb
from django_filters import rest_framework as filters
from django.db.models import Q, F, Func
from django.contrib.auth.models import User
from django.views.generic import ListView

from rest_framework import generics, pagination, status
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny

from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.views import APIView
from rest_framework import viewsets, response, permissions

from .my_pagination import DataproductsPagination

from .models import RIGHTS_PUBLIC, RIGHTS_SECURE, RIGHTS_PROPRIETARY

from .models import Agent, Process, Activity, Project, DataEntity, DataProduct, Entity, Observation, TextEntity, \
    Parset, AltaObject, Ingest, ObservationCoordinates, ObservationCoordinatesGlobe, AltaFile, Release
from .serializers import AgentSerializer, ActivitySerializer, ProcessSerializer, ProjectSerializer, \
    DataEntitySerializer, DataProductSerializer, EntitySerializer, ObservationSerializer, TextEntitySerializer, \
    ParsetSerializer, AltaObjectSerializer, IngestSerializer, UserSerializer, ObservationCoordinatesSerializer, \
    AltaFileSerializer, ObservationFlatSerializer, ActivityFlatSerializer, IngestFlatSerializer, ReleaseSerializer, \
    DataProductFlatSerializer, GetStorageSerializer

from .services.alta_utils import timeit
from .services import algorithms

datetime_format_string = '%Y-%m-%dT%H:%M:%SZ'
logger = logging.getLogger(__name__)


# generic class-based views, a way to hide the different http methods, see DRF documentation.
# (in case you wonder where all the get, post, ... methods and the response are)

class ReleaseFilter(filters.FilterSet):
    # altapi/releases?PID=123

    class Meta:
        model = Release

        fields = {
            'release_id': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
            'releaseDate': ['gt', 'lt', 'gte', 'lte', 'contains', 'exact', 'year__lt', 'year__gt'],
            'project__externalRef': ['exact', 'in', 'icontains'],
        }


class IngestFilter(filters.FilterSet):
    # altapi/ingests?PID=123

    class Meta:
        model = Ingest

        fields = {
            'PID': ['exact', 'icontains'],
            'status': ['exact', 'icontains', 'in'],  # /altapi/ingests?&status__in=ARCHIVED,FAILED
            'username': ['exact', 'icontains'],
            'timestamp': ['gt', 'lt', 'gte', 'lte', 'contains', 'exact', 'year__lt', 'year__gt', 'month__lt',
                          'month__gt'],
        }


class IngestListView(generics.ListCreateAPIView):
    queryset = Ingest.objects.all()
    serializer_class = IngestSerializer
    filter_class = IngestFilter


class IngestDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ingest.objects.all()
    serializer_class = IngestSerializer


class IngestFlatListView(generics.ListCreateAPIView):
    queryset = Ingest.objects.all()
    serializer_class = IngestFlatSerializer
    filter_class = IngestFilter


class IngestFlatDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ingest.objects.all()
    serializer_class = IngestFlatSerializer


class AltaObjectFilter(filters.FilterSet):
    # altapi/altaobjects?type=dataproduct

    class Meta:
        model = AltaObject

        fields = {
            'type': ['exact', 'icontains'],  # http://localhost:8000/altapi/altaobjects?&type=activity
            # 'ingest': ['exact', 'icontains']
        }


class AltaObjectListView(generics.ListCreateAPIView):
    queryset = AltaObject.objects.all()
    serializer_class = AltaObjectSerializer
    filter_class = AltaObjectFilter


class AltaObjectDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = AltaObject.objects.all()
    serializer_class = AltaObjectSerializer


class AgentFilter(filters.FilterSet):
    # altapi/agents?uri=123

    class Meta:
        model = Agent

        fields = {
            'externalRef': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
        }


class AgentListView(generics.ListCreateAPIView):
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer
    filter_class = AgentFilter


class AgentDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer


class ProcessFilter(filters.FilterSet):
    # altapi/processes?uri={my_process_uri}

    class Meta:
        model = Process

        fields = {
            'uri': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
        }


class ProcessListView(generics.ListCreateAPIView):
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer
    filter_class = ProcessFilter


class ProcessDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer


class ActivityFilter(filters.FilterSet):
    # altapi/activities?runId={id}

    # nv: 8 aug 2019, this broke after upgrade django-filter from 1.0.0 => 2.0.0
    # https://django-filter.readthedocs.io/en/master/guide/migration.html#filter-name-renamed-to-filter-field-name-792
    datasetIDs = django_filters.CharFilter(field_name="datasetID", lookup_expr='icontains')

    class Meta:
        model = Activity

        fields = {
            'runId': ['exact', 'in', 'icontains', 'gt', 'lt', 'gte', 'lte'],
            'datasetID': ['exact', 'in', 'icontains'],
            'process__type': ['exact', 'in', 'icontains'],
            'process__uri': ['exact', 'in', 'icontains'],
            'quality': ['exact', 'in', 'icontains'],
            'max_lifetime_on_disk': ['gt', 'lt', 'gte', 'lte', 'exact'],
            'locality_policy': ['exact', 'in', 'icontains'],
            'locality_status': ['exact', 'in', 'icontains'],
            'release__release_id': ['exact', 'in', 'icontains'],
            'project__externalRef': ['exact', 'in', 'icontains'],
        }



def get_queryset_auth(object, my_model_class, process_type=None):
    """
    This is a global function that can be used to override the get_queryset for a model
    It is specifically used to return a different queryset for anonymous or authenticated users based
    on the 'rights' field in different models (Projects, Observations, Activities, Releases, Dataproducts)
    For example, see in the ReleaseListView how the default get_queryset is overridden,
    the result will be that anonymous users will only see 'public' releases in the 'Release' button of the GUI.

    :param object:
    :param my_model_class:
    :return:
    """
    user = str(object.request.user)
    auth = str(object.request.auth)
    logger.info("user (auth) : " + user + ' (' + auth + ')')

    # Authorisation: Anonymous users only see 'public' dataproducts
    if (object.request.user.is_superuser):
        # superusers can see everything
        # logger.info("admin user : " + str(object.request.user))
        if (process_type != None):
            queryset = my_model_class.objects.filter(process__type__icontains=process_type)
        else:
            queryset = my_model_class.objects.all()

    else:
        if (user == 'AnonymousUser'):
            # AnonymousUser (not logged in) can see only public dataproducts
            # queryset = my_model_class.objects.filter(rights=RIGHTS_PUBLIC)
            if (process_type!=None):
                queryset = my_model_class.objects.filter(Q(rights=RIGHTS_PUBLIC), process__type__icontains=process_type)
            else:
                queryset = my_model_class.objects.filter(Q(rights=RIGHTS_PUBLIC))
        else:
            # All logged in users can see public and secure data
            if (process_type!=None):
                queryset = my_model_class.objects.filter(Q(rights=RIGHTS_PUBLIC) | Q(rights=RIGHTS_SECURE), process__type__icontains=process_type)
            else:
                queryset = my_model_class.objects.filter(Q(rights=RIGHTS_PUBLIC) | Q(rights=RIGHTS_SECURE) )

    return queryset


class ActivityListView(generics.ListCreateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    filter_class = ActivityFilter

    # overriding GET get_queryset to access the request
    def get_queryset(self):
        return get_queryset_auth(self, Activity)


class ActivityDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    # overriding GET get_queryset to access the request
    def get_queryset(self):
        return get_queryset_auth(self, Activity)

    def put(self, request, *args, **kwargs):
        logger.info("PUT: Activity")
        return self.update(request, *args, **kwargs)


class ActivityFlatListView(generics.ListCreateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivityFlatSerializer
    filter_class = ActivityFilter

    # overriding GET get_queryset to access the request
    def get_queryset(self):
        return get_queryset_auth(self, Activity)


class ActivityFlatDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivityFlatSerializer

    # overriding GET get_queryset to access the request
    def get_queryset(self):
        return get_queryset_auth(self, Activity)

    def put(self, request, *args, **kwargs):
        logger.info("PUT: Activity")
        return self.update(request, *args, **kwargs)


class ObservationFilter(filters.FilterSet):
    # altapi/observations?runId={id}

    class Meta:
        model = Observation

        fields = {
            'id': ['exact'],
            'target': ['exact', 'in', 'icontains'],
            'datasetID': ['exact', 'in', 'icontains'],
            'runId': ['exact', 'in', 'icontains','gt', 'lt', 'gte', 'lte'],
            'startTime': ['gt', 'lt', 'gte', 'lte', 'contains', 'exact', 'year__lt', 'year__gt'],
            'endTime': ['gt', 'lt', 'gte', 'lte', 'contains', 'exact', 'year__lt', 'year__gt'],
            'calibrator_target': ['exact'],
            'quality': ['exact', 'in', 'icontains'],
            'success': ['exact', 'in', 'icontains'],
            'max_lifetime_on_disk': ['gt', 'lt', 'gte', 'lte', 'exact'],
            'locality_policy': ['exact', 'in', 'icontains'],
            'locality_status': ['exact', 'in', 'icontains'],
            'release__release_id': ['exact', 'in', 'icontains'],
            'comment': ['exact', 'in', 'icontains'],
            'instrument': ['exact', 'in', 'icontains'],
            'project__externalRef': ['exact', 'in', 'icontains'],
        }

    @property
    def qs(self):
        # override the primary queryset .qs
        # https://django-filter.readthedocs.io/en/stable/guide/usage.html#filtering-the-primary-qs
        parent = super(ObservationFilter, self).qs
        cone_filter = self.get_cone_filter()
        return parent.filter(cone_filter)

    def get_cone_filter(self):
        # This function applies a filter to the observations: it only returns observations that are (partially) in a part of the sky (a 'view').
        # The filter determines if the cone of the view and the cone of an observation overlap. If so, the observation is in view. Cones are defined by a combination of RA/dec and FoV.
        # This implementation it might include some false positives that are close to the view. This is easier to implement as a django filter. See below for a precise definition of overlap.

        # This implementation defines overlap as follows:
        # By applying RA - (0.5 * FoV), we get the lower edge of a cone, because 0.5 * FoV is the diameter of the circle. By applying RA + (0.5 * FoV), we get the upper edge. These edges can be calculated for both observation and view.
        # The RA of an observation and view overlap:
        #   - if view_RA_lower_edge <= observation_RA_lower_edge <= view_RA_upper_edge, or
        #   - if similarly view_RA_lower_edge <= observation_RA_upper_edge <= view_RA_upper_edge, or
        #   - if observation_RA_lower_edge <= view_RA_lower_edge <=  view_RA_upper_edge <= observation_RA_upper_edge
        # The first two conditions state that one of the observation edges are positioned between the view edges, and the third condition states that the view is fully contained within the observation edges.

        # We defined the above conditions for the relationship of RA and FoV between an observation and view. By replacing RA with dec, we define the same relationship of dec and FoV between an observation and view.

        # This implementation filters the relationships RA,FoV and dec,FoV independently.
        # This can be interpreted as considering the observation and view cones as squares on a flattened sky, where the RA and dec are X and Y coordinates, and the FoV is the length of the edges of the squares.
        # The precise definition of overlap at the bottom of this function interprets the cones correctly as circles. Overlap is then determined by using cos() sin() and arccos().

        # RA/dec + FoV of the part of the sky with default values
        view_ra = float(self.request.query_params.get('view_ra', 0))
        view_dec = float(self.request.query_params.get('view_dec', 0))
        view_fov = float(self.request.query_params.get('view_fov', 360))

        if view_fov == 360:
            # no need to filter if the view contains the whole sky. maybe the view parameters were not passed
            return Q()

        # RA/dec columns in database
        obs_dec = F('dec')
        obs_ra = F('RA')

        # the two filters for RA and dec
        ra_filter = ObservationFilter.get_single_angle_filter(obs_ra, view_ra, view_fov)
        dec_filter = ObservationFilter.get_single_angle_filter(obs_dec, view_dec, view_fov)
        # intersect the conditionals for ra, dec
        return ra_filter & dec_filter

        # A more precise overlap definition:
        #
        # Max angle:
        # To determine overlap between two cones A and B, we define a maximum angle between the centers of A and B:
        # max angle = (angle between center of A and its circumference) + (angle between center of B and its circumference)
        # In other words, the maximum angle is found by placing the two circles agains each other, and measure the angle between the centers.
        #
        # Actual angle between centers of view and observation:
        # The actual angle between two cones is defined by the angle between the two RA/dec pairs.
        # A precise formula is defined here:
        #    - https://physics.stackexchange.com/a/224952
        #    - http://www.gyes.eu/calculator/calculator_page1.htm
        # cos(A) = sin(Decl.1)sin(Decl.2) + cos(Decl.1)cos(Decl.2)cos(RA.1 - RA.2) and thus, A = arccos(A)
        #
        # A view and observation overlap if actual angle <= max angle

    @staticmethod
    def get_single_angle_filter(obs_angle, view_angle, view_fov):
        # this function filters observations that are within view with respect to a single angle (RA or dec).
        # to filter observations within a view, this functions needs to be called independently for RA and dec
        # arg obs_angle: the RA or dec database field: F('RA') or F('dec')
        # arg view_angle: the RA or dec of the view, as defined by a query parameter
        # arg ob_fov: the FoV database field: F('fov')
        # arg view_fov: the FoV of the view, as defined by a query parameter

        # This calculation is made more complex by having to apply mod 360 degrees, and by lower edge > upper edge if a cone contains 0 degrees.
        # Therefore, we start by adjusting the view, so that view_lower == 0 degrees. this way, we need to apply less mod() and abs() and conditionals
        view_lower = 0
        view_upper = view_fov
        view_radius = (0.5 * view_fov)
        distance_to_0_degrees = view_angle - view_radius
        adjustment = -distance_to_0_degrees

        # calculate an adjusted obs_upper and obs_lower
        obs_fov = F('fov')
        obs_radius = 0.5 * obs_fov
        obs_upper = mod_expression(obs_angle + obs_radius + adjustment, 360)
        obs_lower = mod_expression(obs_angle - obs_radius + adjustment, 360)

        def greater_equals_zero(expr):
            # Q does not support a complex expression or a constant value on the left hand side of a conditional; the left hand side must be a field, so we rewrite 'expr >= 0' as 'fov <= expr + fov' as a work-around
            return Q(fov__lte=expr + obs_fov)

        return (
            #  obs_lower is between view_lower and view_upper:
            #       view_lower <= obs_lower <= view_upper ->
            #       # view_lower == 0
            #       obs_lower <= view_upper ->
            #       0 <= view_upper - obs_lower ->
            greater_equals_zero(view_upper - obs_lower) |
            #  obs_upper is between view_lower and view_upper, similar to previous conditional:
            greater_equals_zero(view_upper - obs_upper) |
            (
                # the observation fully contains the view:
                #   - obs_lower > obs_upper: obs_lower is adjusted to below 0 degrees (mod 360), because view_lower == 0 and obs_lower '<' view_lower:
                greater_equals_zero(obs_lower - obs_upper) & Q(fov__gt=0) &
                #   - view_upper <= obs_upper :
                greater_equals_zero(obs_upper - view_upper)
            )
        )


def mod_expression(value, modulus):
    # utility function: a custom value mod 360 implementation, because the call to Func(value, 360, function='MOD') returns 'Exception Value: function mod(double precision, integer) does not exist'
    # it's not clear how to cast the arguments to call MOD(numeric, numeric) http://www.leadum.com/downloads/dbscribe/samples/postgresql/web_modern/function/main/1068512810.html
    times = Func((value + modulus) / modulus, function='FLOOR')
    return (value + modulus) - (times * modulus)


class ObservationListView(generics.ListCreateAPIView):
    queryset = Observation.objects.all().distinct()
    serializer_class = ObservationSerializer
    filter_class = ObservationFilter

    # overriding GET get_queryset to access the request
    @timeit
    def get_queryset(self):
        # logger.info("ObservationListView.get_queryset(" + str(self.request.query_params)+")")
        return get_queryset_auth(self, Observation)


class ObservationDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Observation.objects.all()
    serializer_class = ObservationSerializer


class ObservationFlatListView(generics.ListCreateAPIView):
    queryset = Observation.objects.all()
    serializer_class = ObservationFlatSerializer
    filter_class = ObservationFilter

    # overriding GET get_queryset to access the request
    @timeit
    def get_queryset(self):
        # logger.info("ObservationListView.get_queryset(" + str(self.request.query_params)+")")
        return get_queryset_auth(self, Observation)


class ObservationFlatDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Observation.objects.all()
    serializer_class = ObservationFlatSerializer


class ObservationFlatDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Observation.objects.all()
    serializer_class = ObservationFlatSerializer


class PipelineListView(generics.ListCreateAPIView):
    # queryset = Activity.objects.filter(process__type__icontains='pipeline')
    serializer_class = ActivitySerializer
    filter_class = ActivityFilter

    # overriding GET get_queryset to access the request
    def get_queryset(self):
        return get_queryset_auth(self, Activity, process_type="pipeline")


class PipelineFlatListView(generics.ListCreateAPIView):
    #queryset = Activity.objects.filter(process__type__icontains='pipeline')
    serializer_class = ActivityFlatSerializer
    filter_class = ActivityFilter

    # overriding GET get_queryset to access the request
    @timeit
    def get_queryset(self):
        return get_queryset_auth(self, Activity, process_type="pipeline")


class ProjectFilter(filters.FilterSet):
    # altapi/projects?externalRef=my_project_reference

    class Meta:
        model = Project

        fields = {
            'externalRef': ['exact', 'icontains'],
            'rights': ['exact', 'icontains'],
        }


class ProjectListView(generics.ListCreateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    filter_class = ProjectFilter


class ProjectDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class EntityFilter(filters.FilterSet):
    # altapi/entities?PID={pid}

    class Meta:
        model = Entity

        fields = {
            'PID': ['exact']
        }


class EntityListView(generics.ListCreateAPIView):
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer
    filter_class = EntityFilter


class EntityDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer


class DataEntityListView(generics.ListCreateAPIView):
    queryset = DataEntity.objects.all()
    serializer_class = DataEntitySerializer
    filter_class = EntityFilter


class DataEntityDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = DataEntity.objects.all()
    serializer_class = DataEntitySerializer


class TextEntityListView(generics.ListCreateAPIView):
    queryset = TextEntity.objects.all()
    serializer_class = TextEntitySerializer
    filter_class = EntityFilter


class TextEntityDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = TextEntity.objects.all()
    serializer_class = TextEntitySerializer


class ParsetListView(generics.ListCreateAPIView):
    queryset = Parset.objects.all()
    serializer_class = ParsetSerializer


class ParsetDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Parset.objects.all()
    serializer_class = ParsetSerializer


class DataProductFilter(filters.FilterSet):
    # nv:23jun2017 - this class implements the Filter and Query options for the Dataproduct API.

    # nv: 8 aug 2019, this broke after upgrade django-filter from 1.0.0 => 2.0.0
    # https://django-filter.readthedocs.io/en/master/guide/migration.html#filter-name-renamed-to-filter-field-name-792
    creationTimeContains = django_filters.CharFilter(field_name="creationTime", lookup_expr='contains')

    # the Meta tag is used to generate filters automatically
    class Meta:
        model = DataProduct

        name = django_filters.AllValuesFilter()

        fields = {
            # ../query?id__gt=5&id__lt=10
            'id': ['lt', 'gt'],
            # ../query?dataProductType=IMAGE
            # ../query?dataProductType__in=IMAGE,CUBE
            'dataProductType': ['exact', 'in'],
            'storageRef': ['exact', 'in'],
            # ../query?dataProductSubType=continuumCube
            # ../query?dataProductSubType__in=continuumCube,lineCube
            'dataProductSubType': ['exact', 'in'],
            'calibrationLevel': ['exact'],
            'title': ['exact', 'icontains'],
            'format': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],

            'creationTime': ['gt', 'lt', 'gte', 'lte', 'contains', 'exact', 'year__lt', 'year__gt', 'month__lt',
                             'month__gt'],
            'project_id': ['exact', 'in'],  # ../query?project_id=2
            'datasetID': ['exact', 'in'],  # ../query?datasetID=12345
            # ../query?project_id__in=1,3

            'PID': ['exact', 'icontains'],
            'generatedByActivity__runId': ['exact', 'in', 'icontains'],
            'locality': ['exact', 'in', 'icontains'],

            'release__release_id': ['exact', 'in', 'icontains'],
            'project__externalRef': ['exact', 'in', 'icontains'],
        }

    @property
    def qs(self):
        # override the primary queryset .qs
        # https://django-filter.readthedocs.io/en/stable/guide/usage.html#filtering-the-primary-qs

        parent = super(DataProductFilter, self).qs
        cone_filter = self.get_cone_filter()
        return parent.filter(cone_filter)

    def get_cone_filter(self):
        # This function applies a filter to the observations: it only returns observations that are (partially) in a part of the sky (a 'view').
        # The filter determines if the cone of the view and the cone of an observation overlap. If so, the observation is in view. Cones are defined by a combination of RA/dec and FoV.
        # This implementation it might include some false positives that are close to the view. This is easier to implement as a django filter. See below for a precise definition of overlap.

        # This implementation defines overlap as follows:
        # By applying RA - (0.5 * FoV), we get the lower edge of a cone, because 0.5 * FoV is the diameter of the circle. By applying RA + (0.5 * FoV), we get the upper edge. These edges can be calculated for both observation and view.
        # The RA of an observation and view overlap:
        #   - if view_RA_lower_edge <= observation_RA_lower_edge <= view_RA_upper_edge, or
        #   - if similarly view_RA_lower_edge <= observation_RA_upper_edge <= view_RA_upper_edge, or
        #   - if observation_RA_lower_edge <= view_RA_lower_edge <=  view_RA_upper_edge <= observation_RA_upper_edge
        # The first two conditions state that one of the observation edges are positioned between the view edges, and the third condition states that the view is fully contained within the observation edges.

        # We defined the above conditions for the relationship of RA and FoV between an observation and view. By replacing RA with dec, we define the same relationship of dec and FoV between an observation and view.

        # This implementation filters the relationships RA,FoV and dec,FoV independently.
        # This can be interpreted as considering the observation and view cones as squares on a flattened sky, where the RA and dec are X and Y coordinates, and the FoV is the length of the edges of the squares.
        # The precise definition of overlap at the bottom of this function interprets the cones correctly as circles. Overlap is then determined by using cos() sin() and arccos().

        # RA/dec + FoV of the part of the sky with default values
        view_ra = float(self.request.query_params.get('view_ra', 0))
        view_dec = float(self.request.query_params.get('view_dec', 0))
        view_fov = float(self.request.query_params.get('view_fov', 360))

        if view_fov == 360:
            # no need to filter if the view contains the whole sky. maybe the view parameters were not passed
            return Q()

        # RA/dec columns in database
        obs_dec = F('dec')
        obs_ra = F('RA')

        # the two filters for RA and dec
        ra_filter = DataProductFilter.get_single_angle_filter(obs_ra, view_ra, view_fov)
        dec_filter = DataProductFilter.get_single_angle_filter(obs_dec, view_dec, view_fov)
        # intersect the conditionals for ra, dec
        return ra_filter & dec_filter

        # A more precise overlap definition:
        #
        # Max angle:
        # To determine overlap between two cones A and B, we define a maximum angle between the centers of A and B:
        # max angle = (angle between center of A and its circumference) + (angle between center of B and its circumference)
        # In other words, the maximum angle is found by placing the two circles agains each other, and measure the angle between the centers.
        #
        # Actual angle between centers of view and observation:
        # The actual angle between two cones is defined by the angle between the two RA/dec pairs.
        # A precise formula is defined here:
        #    - https://physics.stackexchange.com/a/224952
        #    - http://www.gyes.eu/calculator/calculator_page1.htm
        # cos(A) = sin(Decl.1)sin(Decl.2) + cos(Decl.1)cos(Decl.2)cos(RA.1 - RA.2) and thus, A = arccos(A)
        #
        # A view and observation overlap if actual angle <= max angle

    @staticmethod
    def get_single_angle_filter(obs_angle, view_angle, view_fov):
        # this function filters observations that are within view with respect to a single angle (RA or dec).
        # to filter observations within a view, this functions needs to be called independently for RA and dec
        # arg obs_angle: the RA or dec database field: F('RA') or F('dec')
        # arg view_angle: the RA or dec of the view, as defined by a query parameter
        # arg ob_fov: the FoV database field: F('fov')
        # arg view_fov: the FoV of the view, as defined by a query parameter

        # This calculation is made more complex by having to apply mod 360 degrees, and by lower edge > upper edge if a cone contains 0 degrees.
        # Therefore, we start by adjusting the view, so that view_lower == 0 degrees. this way, we need to apply less mod() and abs() and conditionals
        view_lower = 0
        view_upper = view_fov
        view_radius = (0.5 * view_fov)
        distance_to_0_degrees = view_angle - view_radius
        adjustment = -distance_to_0_degrees

        # calculate an adjusted obs_upper and obs_lower
        obs_fov = F('fov')
        obs_radius = 0.5 * obs_fov
        obs_upper = mod_expression(obs_angle + obs_radius + adjustment, 360)
        obs_lower = mod_expression(obs_angle - obs_radius + adjustment, 360)

        def greater_equals_zero(expr):
            # Q does not support a complex expression or a constant value on the left hand side of a conditional; the left hand side must be a field, so we rewrite 'expr >= 0' as 'fov <= expr + fov' as a work-around
            return Q(fov__lte=expr + obs_fov)

        return (
            #  obs_lower is between view_lower and view_upper:
            #       view_lower <= obs_lower <= view_upper ->
            #       # view_lower == 0
            #       obs_lower <= view_upper ->
            #       0 <= view_upper - obs_lower ->
            greater_equals_zero(view_upper - obs_lower) |
            #  obs_upper is between view_lower and view_upper, similar to previous conditional:
            greater_equals_zero(view_upper - obs_upper) |
            (
                # the observation fully contains the view:
                #   - obs_lower > obs_upper: obs_lower is adjusted to below 0 degrees (mod 360), because view_lower == 0 and obs_lower '<' view_lower:
                greater_equals_zero(obs_lower - obs_upper) & Q(fov__gt=0) &
                #   - view_upper <= obs_upper :
                greater_equals_zero(obs_upper - view_upper)
            )
        )

# a custom pagination class for dataproducts
class DataproductsPagination_obsolete(pagination.PageNumberPagination):
    """
    a custom pagination class for dataproducts
    because 100 is not enough for ARTS_SC4 TAB mode which has 400+ dataproducts
    """
    page_size = 500


class DataProductListView(generics.ListCreateAPIView):
    serializer_class = DataProductSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # using the Django Filter Backend - https://django-filter.readthedocs.io/en/latest/index.html
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DataProductFilter

    # dataproducts have a custom pagination class,
    pagination_class = DataproductsPagination

    # overriding GET get_queryset to access the request
    def get_queryset(self):

        logger.info("query_params = " + str(self.request.query_params))
        # logger.info("request.data = " + str(self.request.data))

        user = str(self.request.user)
        auth = str(self.request.auth)
        logger.info("user (auth) : " + user + ' (' + auth + ')')

        # Authorisation: Anonymous users only see 'public' dataproducts
        if (self.request.user.is_superuser):
            # superusers can see everything
            logger.info("admin user : " + str(self.request.user))
            queryset = DataProduct.objects.all().order_by('id')
        else:
            if (user == 'AnonymousUser'):
                # AnonymousUser (not logged in) can see only public dataproducts
                queryset = DataProduct.objects.filter(rights=RIGHTS_PUBLIC).order_by('id')
            else:
                # All logged in users can see public and secure dataproducts
                queryset = DataProduct.objects.filter(Q(rights=RIGHTS_PUBLIC) | Q(rights=RIGHTS_SECURE)).order_by('id')

        return queryset

    # overriding the POST that is used to create new records (demo)
    def perform_create(self, serializer):
        logger.info("perform_create():")
        logger.info("dataproduct created by user : " + str(self.request.user))
        serializer.save()


class DataProductDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = DataProduct.objects.all()
    serializer_class = DataProductSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # overriding GET get_queryset to access the request
    def get_queryset(self):

        logger.info("query_params = " + str(self.request.query_params))
        logger.info("request.data = " + str(self.request.data))

        user = str(self.request.user)
        auth = str(self.request.auth)
        logger.info("user (auth) : " + user + ' (' + auth + ')')

        # Authorisation: Anonymous users only see 'public' dataproducts
        if (self.request.user.is_superuser):
            # superusers can see everything
            logger.info("admin user : " + str(self.request.user))
            queryset = DataProduct.objects.all()
        else:
            if (user == 'AnonymousUser'):
                # AnonymousUser (not logged in) can see only public dataproducts
                queryset = DataProduct.objects.filter(rights=RIGHTS_PUBLIC)
            else:
                # All logged in users can see public and secure dataproducts
                queryset = DataProduct.objects.filter(
                    Q(rights=RIGHTS_PUBLIC) | Q(rights=RIGHTS_SECURE))

        return queryset

    # override the put (from the mixins), to be able to access the request and its methods.
    # http://www.django-rest-framework.org/api-guide/generic-views/
    def put(self, request, *args, **kwargs):
        logger.info("PUT: DataProduct")

        # examples of accessing information from the request
        logger.info("request.query_params = " + str(self.request.query_params))
        logger.info("request.data = " + str(self.request.data))
        logger.info("request.user = " + str(self.request.user))
        return self.update(request, *args, **kwargs)

    def perform_update(self, serializer):
        logger.info("perform_update():")
        serializer.save()
        logger.info("dataproduct updated by user : " + str(self.request.user))


class DataProductFlatListView(generics.ListCreateAPIView):
    serializer_class = DataProductFlatSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # using the Django Filter Backend - https://django-filter.readthedocs.io/en/latest/index.html
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DataProductFilter

    # dataproducts have a custom pagination class,
    pagination_class = DataproductsPagination

    # overriding GET get_queryset to access the request
    def get_queryset(self):

        logger.info("query_params = " + str(self.request.query_params))
        # logger.info("request.data = " + str(self.request.data))

        user = str(self.request.user)
        auth = str(self.request.auth)
        logger.info("user (auth) : " + user + ' (' + auth + ')')

        # Authorisation: Anonymous users only see 'public' dataproducts
        if (self.request.user.is_superuser):
            # superusers can see everything
            logger.info("admin user : " + str(self.request.user))
            queryset = DataProduct.objects.all().order_by('id')
        else:
            if (user == 'AnonymousUser'):
                # AnonymousUser (not logged in) can see only public dataproducts
                queryset = DataProduct.objects.filter(rights=RIGHTS_PUBLIC).order_by('id')
            else:
                # All logged in users can see public and secure dataproducts
                queryset = DataProduct.objects.filter(Q(rights=RIGHTS_PUBLIC) | Q(rights=RIGHTS_SECURE)).order_by('id')

        return queryset

    # overriding the POST that is used to create new records (demo)
    def perform_create(self, serializer):
        logger.info("perform_create():")
        logger.info("dataproduct created by user : " + str(self.request.user))
        serializer.save()


class DataProductFlatDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = DataProduct.objects.all()
    serializer_class = DataProductFlatSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # overriding GET get_queryset to access the request
    def get_queryset(self):

        logger.info("query_params = " + str(self.request.query_params))
        logger.info("request.data = " + str(self.request.data))

        user = str(self.request.user)
        auth = str(self.request.auth)
        logger.info("user (auth) : " + user + ' (' + auth + ')')

        # Authorisation: Anonymous users only see 'public' dataproducts
        if (self.request.user.is_superuser):
            # superusers can see everything
            logger.info("admin user : " + str(self.request.user))
            queryset = DataProduct.objects.all()
        else:
            if (user == 'AnonymousUser'):
                # AnonymousUser (not logged in) can see only public dataproducts
                queryset = DataProduct.objects.filter(rights=RIGHTS_PUBLIC)
            else:
                # All logged in users can see public and secure dataproducts
                queryset = DataProduct.objects.filter(
                    Q(rights=RIGHTS_PUBLIC) | Q(rights=RIGHTS_SECURE))

        return queryset

    # override the put (from the mixins), to be able to access the request and its methods.
    # http://www.django-rest-framework.org/api-guide/generic-views/
    def put(self, request, *args, **kwargs):
        logger.info("PUT: DataProduct")

        # examples of accessing information from the request
        logger.info("request.query_params = " + str(self.request.query_params))
        logger.info("request.data = " + str(self.request.data))
        logger.info("request.user = " + str(self.request.user))
        return self.update(request, *args, **kwargs)

    def perform_update(self, serializer):
        logger.info("perform_update():")
        serializer.save()
        logger.info("dataproduct updated by user : " + str(self.request.user))


class UserListView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)


class ReleaseListView(generics.ListCreateAPIView):
    queryset = Release.objects.all()
    serializer_class = ReleaseSerializer
    filter_class = ReleaseFilter
    # permission_classes = (permissions.IsAuthenticated,)

    # overriding GET get_queryset so that the results can be filtered on 'rights'
    def get_queryset(self):
        return get_queryset_auth(self, Release)


class ReleaseDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Release.objects.all()
    serializer_class = ReleaseSerializer

    # this view is used to edit the release and set permissions, need authentication.
    permission_classes = (permissions.IsAuthenticated,)


# a custom pagination class to return all observation coordinates
class ObservationCoordinatesPagination(pagination.PageNumberPagination):
    page_size = 10000


# Observation Coordinates for Aladin
class ObservationCoordinatesListView(generics.ListCreateAPIView):
    queryset = ObservationCoordinates.objects.all().distinct()
    serializer_class = ObservationCoordinatesSerializer
    pagination_class = ObservationCoordinatesPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ObservationFilter


# Observation Coordinates for WebGL Globe experiment
# class ObservationCoordinatesGlobeListView(ListView):
class ObservationCoordinatesGlobeListView(generics.ListCreateAPIView):
    # use the ObservationCoordinates Model, which is a proxy model of Observation
    queryset = ObservationCoordinatesGlobe.objects.all().distinct()
    pagination_class = ObservationCoordinatesPagination

    # override list and generate a custom response
    def list(self, request, *args, **kwargs):
        # logger.info("ObservationCoordinatesListView.list()")
        queryset = self.get_queryset()

        # interate queryset to get the coordinates in a format that fit the WebGlobe application.

        my_raw_list = []
        my_calibrated_list = []
        my_image_list = []
        for observation in queryset:
            # logger.info(str(observation))
            my_raw_list.append(observation.dec)
            my_raw_list.append(observation.derived_longitude)
            my_raw_list.append(observation.derived_duration / 20)

            my_calibrated_list.append(observation.dec)
            my_calibrated_list.append(observation.derived_longitude)
            my_calibrated_list.append(observation.derived_duration / 10)

            my_image_list.append(observation.dec)
            my_image_list.append(observation.derived_longitude)
            my_image_list.append(observation.derived_duration)

        raw = ['raw', my_raw_list]
        calibrated = ['calibrated', my_calibrated_list]
        image = ['image', my_image_list]

        my_globe_data = [raw, calibrated, image]

        return Response({
            'description': 'CoordinateList for WebGlobe',
            'alta-globe-data': my_globe_data,
        })

    # required by 'generateschema', but this is now a dummy
    def get_serializer_class(self):
        return ObservationCoordinatesSerializer


# view for uploading files (thumbnails) to the alta-static website
class AltaFileView(APIView):
    # https://stackoverflow.com/questions/4894976/django-custom-file-storage-system
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
    serializer_class = AltaFileSerializer
    queryset = AltaFile.objects.all()

    def post(self, request, *args, **kwargs):
        # http://www.django-rest-framework.org/api-guide/parsers/
        logger.info('AltaFileView.post()')

        file_serializer = AltaFileSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# overview of the uploads
class UploadsView(generics.ListCreateAPIView):
    serializer_class = AltaFileSerializer
    queryset = AltaFile.objects.all()


# details of one uploaded file
class UploadsDetailsView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AltaFileSerializer
    queryset = AltaFile.objects.all()


# --- specialized queries
class GetFreezeView(generics.ListAPIView):
    """
    Retrieve a list of activities that need to be moved to cold storage by irods
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """
        result = algorithms.get_freeze()
        #    {
        #        "storage": [
        #            "190812001",
        #            "190727041"
        #        ]
        #    }

        return Response({
            'storage': result
        })


class GetPruneView(generics.ListAPIView):
    """
    Retrieve a list of activities that need to be moved to cold storage by irods
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """
        result = algorithms.get_prune()
        #    {
        #        "prune": [
        #            "190812001",
        #            "190727041"
        #        ]
        #    }

        return Response({
            'prune': result
        })



class GetMarkedForDeletionView(generics.ListAPIView):
    """
    Retrieve a list of activities that marked for deletion by the irods cleanup scheduler
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """
        result = algorithms.get_marked_for_deletion()
        #    {
        #        "storage": [
        #            "190812001",
        #            "190727041"
        #        ]
        #    }

        return Response({
            'storage': result
        })


class GetScrubView(generics.ListAPIView):
    """
    Retrieve a list of activities that need to be scrubbed from irods
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        result = algorithms.get_scrub()

        return Response({
            'storage': result
        })

class GetPreparedColdStorageView(generics.ListAPIView):
    """
    Retrieve a list of activities that need to be scrubbed from irods
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        result = algorithms.get_prepared_coldstorage()

        return Response({
            'prepared_coldstorage': result
        })


class GetStorageView(generics.ListAPIView):
    """
    Retrieve a list of activities that need to be handled by the irods storage functionality
    The request can contain the following query parameters:
    locality_status:
    locality_policy__icontains:
    quality:
    scrub_timestamp_lte: the time after which the data will be scrubbed
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        try:
            locality_status = self.request.query_params['locality_status']
        except:
            locality_status = 'inarchive'

        try:
            locality_policy__icontains = self.request.query_params['locality_policy__icontains']
        except:
            locality_policy__icontains = 'cold'

        try:
            quality = self.request.query_params['quality']
        except:
            quality = 'good'

        try:
            scrub_timestamp_lte = self.request.query_params['scrub_timestamp_lte']
        except:
            scrub_timestamp_lte = None

        result = algorithms.get_storage(locality_status, locality_policy__icontains, quality, scrub_timestamp_lte)

        return Response({
            'storage': result
        })


class PutLocalityView(generics.ListAPIView):
    """
    Put the given locality to all the dataproducts belonging to the given taskid
    example; /altapi/put-locality?taskid=190812001&locality=cold_disk
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        locality = self.request.query_params['locality']
        taskid = self.request.query_params['taskid']

        result = algorithms.put_locality(taskid, locality)

        return Response({
            'put-locality': result
        })


class AppendLocalityView(generics.ListAPIView):
    """
    Append a given locality to all the dataproducts belonging to the given taskid
    example: /altapi/append-locality?taskid=190812001&locality=cold_disk
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        locality = self.request.query_params['locality']
        taskid = self.request.query_params['taskid']

        result = algorithms.append_locality(taskid, locality)

        return Response({
            'append-locality': result
        })


class RemoveLocalityView(generics.ListAPIView):
    """
    Remove a given locality from all the dataproducts belonging to the given taskid
    example; /altapi/remove-locality?taskid=190812001&locality=cold_disk
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        locality = self.request.query_params['locality']
        taskid = self.request.query_params['taskid']

        result = algorithms.remove_locality(taskid, locality)

        return Response({
            'remove-locality': result
        })


class ColdStorageView(generics.ListAPIView):
    """
    Overview of the activities in the ColdStorage workflow
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        results = algorithms.get_coldstorage_overview()

        return Response({
            'cold-storage': results
        })


class MarkAsReleaseView(generics.ListAPIView):
    """
    Mark a range of taskid's as belonging to a certain release
    ### --- Parameters ---
    #### taskid_from : first taskid in the range (alphabetically)
    #### taskid_to   : last taskid in the range (alphabetically)
    #### release_id  : id of an existing release object
    ###
    ### --- example ---
    #### https://alta.astron.nl/altapi/mark-as-release?taskid_from=180223002&taskid_to=180223003_Z&release_id=EarlyScience
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    @timeit
    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        try:
            # read the arguments from the query
            taskid_from = self.request.query_params['taskid_from']
            taskid_to = self.request.query_params['taskid_to']
            release_id = self.request.query_params['release_id']

            marked_observations, marked_activities, marked_dataproducts = algorithms.mark_as_release(taskid_from, taskid_to, release_id)

            return Response({
                'marked_observations': marked_observations,
                'marked_activities': marked_activities,
                'marked_dataproducts': marked_dataproducts
            })
        except Exception as error:
            return Response({
                'error': str(error)
            })


class GetActivityDetails(generics.ListAPIView):
    """
    Retrieve certain details of an Activity and its dataproducts for the ingest
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        try:
            runid = self.request.query_params['runid']
            results = algorithms.get_activity_details(runid)
            json_results = json.dumps(results)
            return Response({
                'results': json_results
            })
        except Exception as error:
            return Response({
                'error': str(error)
            })



class GetFinalReleasesView(generics.ListAPIView):
    """
    Query that returns a list of releases with status 'final'.
    The iRODS side will use that as a signal to create symbolic links containing the release_id.
    example: https://alta.astron.nl/altapi/get-final-releases
    """
    queryset = Release.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        result = algorithms.get_releases('final')

        return Response({
            'release': result
        })



class PutReleaseStatusView(generics.ListAPIView):
    """
    Put the release object to a given status given
    example; /altapi/put-release-status?release_id=EarlyScience&status=published
    """
    queryset = Release.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        release_id = self.request.query_params['release_id']
        status = self.request.query_params['status']

        release = Release.objects.get(release_id=release_id)
        release.status = status
        release.save()

        return Response({
            'put-release-status': str(release_id)
        })


class GetDataProductsDetails(generics.ListAPIView):
    """
    Retrieve certain details of an DataProducts; a list of name, Type and SubType
    """
    queryset = Activity.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        try:
            runid = self.request.query_params['runid']
            results = algorithms.get_dataproducts_storageref_type_subtype(runid)
            # json_results = json.dumps(results)
            return Response({
                'results': results
            })
        except Exception as error:
            return Response({
                'error': str(error)
            })


class AppendStorageRefView(generics.ListAPIView):
    """
    Append a given storageref to all the dataproduct identified by PID
    example: altapi/append-storageref?PID=ba5ab9ec-524f-45ed-9100-d4baa00ed3f9&storageref=cold:190812001/L190812001_SB001_uv_raw.MS.TAR

    """
    queryset = DataProduct.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        PID = self.request.query_params['PID']
        storageref = self.request.query_params['storageref']

        result = algorithms.append_storageref(PID, storageref)

        return Response({
            'append-storageref': result
        })


class RemoveStorageRefView(generics.ListAPIView):
    """
    Remove a given storageref from the dataproduct identified by PID
    example: altapi/remove-storageref?PID=ba5ab9ec-524f-45ed-9100-d4baa00ed3f9&storageref=190812001/L190812001_SB001_uv_raw.MS
    """
    queryset = DataProduct.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """

        # read the arguments from the query
        PID = self.request.query_params['PID']
        storageref = self.request.query_params['storageref']

        result = algorithms.remove_storageref(PID, storageref)

        return Response({
            'remove-storageref': result
        })


class AppendStorageRefByNameView(generics.ListAPIView):
    """
    Append a given storageref to all the dataproduct identified by runid and current storageref
    example: altapi/append-storageref-byname?runid=200427001&name=200427001_AP_B039.TAR&storageref=cold:200427001_AP_B039.TAR
    """
    queryset = DataProduct.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """
        # read the arguments from the query
        result = algorithms.append_storageref_byname(self.request.query_params['runid'],
                                                     self.request.query_params['name'],
                                                     self.request.query_params['storageref'])
        return Response({
            'append-storageref': result
        })


class RemoveStorageRefByNameView(generics.ListAPIView):
    """
    Remove a given storageref from the dataproduct identified by runid
    example: altapi/remove-storageref-byname?runid=200427001&name=200427001_AP_B039.TAR&storageref=cold:200427001_AP_B039.TAR
    """
    queryset = DataProduct.objects.all()
    serializer_class = GetStorageSerializer  # dummy

    def list(self, request, *args, **kwargs):
        """
        Overriding the list method so that a custom created json response can be returned.
        This is faster than what the DRF Serializer can do, and better customizable
        :return: Returns a custom response with a json structure
        """
        # read the arguments from the query
        result = algorithms.remove_storageref_byname(self.request.query_params['runid'],
                                                     self.request.query_params['name'],
                                                     self.request.query_params['storageref'])
        return Response({
            'remove-storageref': result
        })