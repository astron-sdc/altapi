from django.apps import AppConfig


class MyAppConfig(AppConfig):
    name = 'apps.api'

# nv:8dec2017 override 'ready' to enable signals
    def ready(self):
        # from . import signals
        from .services import signals